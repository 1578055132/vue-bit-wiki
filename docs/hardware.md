<Cover imgSrc="https://static.igem.wiki/teams/4691/wiki/head-2-hardware.png" />

# Overview

​	Team BIT's goal this year is to design an initiative DNA damage effect evaluation system, a microbial sensor with high sensitivity constructed on E.coli. The engineering bacteria requires laboratory equipment such as shaking incubators and microplate readers, as well as the expertise of trained personnel, during the testing process. Therefore, its application is limited to laboratory settings.

​	In order to meet the demand for versatile and portable applications, we aim to develop a compact, highly integrated, portable, and user-friendly platform. This platform will handle the complete process of engineering strain revival, cultivation, detection, and analysis. The hardware technology roadmap is depicted below.

<ImageAndLabel4 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-1-roadmap.png" Label="Fig.1 Technical roadmap"/>

​	The hardware can be divided into two parts, microfluidic chip and device. During application, the engineering strain is stored in a freeze-dried form within the microfluidic chip culture chambers. The chip is preloaded in the device, awaiting detection. During operation, the device drives a micro-peristaltic pump to inject the culture medium into the chip chambers, facilitating the revival of the engineering strain. Subsequently, the cultivation process begins, with the device maintaining a constant temperature. When the engineering strain reaches a certain growth stage, fluorescence signals are detected. The intensity of the fluorescence signals is captured using a smartphone and a dedicated mobile application, allowing for the evaluation of DNA damage effects. Gif.1shows the chip and device, and the specific operations are demonstrated in Gif.2.




<ImageAndLabel4 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-chip.gif" Label=""/>
<ImageAndLabel4 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-device.gif" Label="Gif.1 Microfluidic Chip and Device"/>

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-demo.gif" Label="Gif.2 Device operation"/>

# Microfluidic Chip

​	We utilize a microfluidic chip as a cultivation platform for our microbial sensor. To investigate the response of the biosensor to DNA damage induced by different concentrations of reagents, this project has designed a static cultivation microfluidic chip with gradient generation functionality. The chip offers advantages such as high throughput, compact structure, and easy fabrication.

​	The chip is made of PMMA, and has three layers. Culture medium and sample(or water) is respectively pumped into the micro channels from different injecting holes. After undergoing a process of splitting and mixing within the microchannel network, a gradient with concentration ratios of 0:0.25:0.5:0.75:1 is ultimately formed. Freeze-dried pellets containing E.coli are preloaded into the lower chamber of the chip and are then reactivated by the mixed sample to respond to the damaging reagents.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-2-chipstructure.png" Label="Fig.2 Chip structure diagram and physical diagram"/>

## Functionality Test

### Sample Injection

​	The device utilizes two micro-peristaltic pumps controlled in parallel for sample injection. The left side of the micro-peristaltic pump is connected to a fluid reservoir bag, while the right side is connected to a silicone hose. In the middle, there is a PP plastic reducing joint, and the other end of the tube is connected to a stainless steel needle, which serves as an injector.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-3-fluiddrivemodule.png" Label="Fig.3 The schematic diagram and physical diagram of the fluid drive module"/>

​	The sample injection functionality was tested using red dye. It was observed that the chip exhibited good fluidity and sealing properties, with no leakage occurring.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-4-chipfunctiontest.jpg" Label="Fig.4 Chip Function Test" scale="0.3"/>

### Concentration gradient generator

​	We simulated the gradient concentration generation effect of the chip using COMSOL software. The simulation produced five concentration gradients: 0: 0.24: 0.5: 0.76: 1.000 (mol/m<sup>3</sup>), consistent with the simulation expectations. Our application of  red dye to the chip also roughly matched the expected results, establishing a general paradigm for reactions with concentration gradients on the chip. We have documented the detailed simulation steps and parameters on the contribution page.



<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/simulation-and-result.png" Label="Fig.5 Simulation and Result"/>

### Constant Temperature Cultivation

​	To meet the constant temperature requirement for the training chambers of the engineering bacteria on the chip, we have developed a temperature control module. As shown in figure 6, the module is composed by three parts: PT1000 thermal sensor, Silicone rubber heating sheet, and a TCM1040 temperature PID control circuit board.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-6-tempcontraldesign.png" Label="Fig.6 Constant Temperature Control Design"/>

​	After setting up the device, the FLUKE51 II thermocouple thermometer is used to monitor the temperature of the silicone heat press pad in real-time. From figure 7, it can be observed that the heating module rapidly increases in temperature shortly after operation and eventually stabilizes at around 37°C.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-7-heatingcurve.jpg" Label="Fig.7 Heating Curve" scale="0.3"/>

​	Randomly select 8 positions on the heating sheet,  temperature tests were conducted respectively at the same time by the FLUKE51 II thermocouple thermometer. The temperatures measured at the eight positions were close to the readings on the sensors, and all eight positions maintained a temperature of around 37°C with a variation range within 0.2°C. This indicates that the silicone rubber heating sheet exhibits good uniformity in heating, ensuring consistent reaction temperatures in all eight chambers.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-8-uniformitytest.png" Label="Fig.8 Uniformity Test Result"/>

​	We'd also managed tests of uniformity and volatility. When temperature reaches 37°C, after maintaining the temperature for 10s, record the temperature every 10s until another 60s has past. The absolute difference between the average and the set temperature is not greater than 0.2°C, indicating that the chamber layer has good temperature control accuracy.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-9-volatilitytest.png" Label="Fig.9 Volatility Test Result"/>

### Fluorescence Detection

​	We constructed a light path to detect the fluorescence emitted by sensor. Currently, mobile phone cameras have features such as zoom and white balance adjustment. The camera can be used as a lens in this optical design, eliminating the need for a separate lens design. This simplifies the optical path and reduces costs.

​	We designed an oblique illumination optical path for the detection system of the microfluidic chip. An LED is used as the excitation light source (488nm) to stimulate the damaged DNA in Escherichia coli, which emits a fluorescence signal. The signal is then collected by the mobile phone camera through collimation and filtering (520nm). The captured image is uploaded to a WeChat mini-program for completion of the detection process.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-10-opticalpathdiagram.png" Label="Fig.10 Optical Path Design" scale="0.3"/>

​	We designed a experiment to check whether our fluorescence detection module detects and reflects the signal of our samples accurately. We prepared a series of fluorescein sodium solutions in different gradients on the glass slides to test, and the result came up with a linearity of the curve, indicating that our module has great quantitativeness and meets the requirement for use.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-11-chipexresult.jpg" Label="Fig.11 Experiment Result on Chip" scale="0.3"/>

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-12-fittingresult.png" Label="Fig.12 Fitting Experiment Results" scale="0.7"/>

## Performance Test

### Bacterial Culture Experiment

​	To test the growth of the engineering bacteria on the chip platform, we added 240μL culture medium and 60μL  engineering bacteria to each of the 8 culture chambers. The chip was placed in a constant temperature incubator at 37°C. The OD600 values of each chamber were measured every 30 minutes, and the analysis is shown in the left of  figure 13.

​	The results have shown that the initial OD value of the bacteria is around 0.13. After cultivating on the chip for 1-3 hours, the OD values stabilize around 0.22-0.26, indicating that the engineering bacteria can grow normally on the chip, and after 1-3 hours of cultivation, the OD values reach the range of 0.2-0.4.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-13-growthodvalue.png" Label="Fig.13 The OD Value and Growth Curve of Bacteria Growth" scale="0.5"/>

### Response to damage reagent of engineering bacteria test

​	We also test the ability of the bacteria on the chip to respond to damaging reagents. In each of the 8 chambers, we added bacterial solution and NA damaging reagent at concentrations 2.5 μM, 5 μM, 7.5 μM, and 10 μM respectively. Then the chip were placed in the temperature constant incubator at 37°C for 2 hours, 200 μL of the culture medium from each chamber was collected for OD value and fluorescence intensity measurements using a microplate reader. The growth time, OD values, and intensity data were recorded.

​	Based on the above results, we tested the response of the engineering bacteria on the platform to the damaging reagent. In each of the eight chambers, we added bacterial solution and different concentrations of sodium napthoquinone-4-sulfonate (a chemical damaging reagent) at concentrations of 2.5 μM, 5 μM, 7.5 μM, and 10 μM, respectively. The chip was incubated at 37°C, and a microplate reader was used to measure the OD600 values and fluorescence intensity of each chamber, calculating the specific fluorescence units (SFU). The results indicate that the engineering bacteria cultured on the chip exhibit a response to the NA damage-inducing reagent, and the response becomes stronger with increasing concentrations of the damaging reagent.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-14-3-bsbiaozheng.png" Label="Fig.14 Chip Experiment and NA Characterization in Bacterial Liquid"/>

### Resuscitation test of freeze-dried engineering bacteria

​	The engineering bacteria were stored in the chambers in the form of freeze-dried powder. A certain amount of culture medium was added to resuscitate the engineering bacteria. After resuscitation, the same concentration gradient of chemical damage-inducing reagent was added. The chip was placed in a constant temperature incubator at 37°C for two hours. A microplate reader was used to measure the OD600 values and fluorescence intensity of each chamber, calculating the specific fluorescence units (SFU). The results indicate that the freeze-dried bacteria, after resuscitation, function normally and exhibit a response to the NA damaging reagent. Furthermore, the response becomes stronger with increasing concentrations of the damaging reagent.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-15-2-dfbiaozheng.png" Label="Fig.15 freeze-dried engineering bacteria and NA Characterization of Freeze-Dried Bacteria"/>

# WeChat Mini-program

​	The device utilizes a WeChat mini-program for user interaction and result analysis. The client-side development, including interface design, image uploading, and API calls, is done using the WeChat Developer Tools. The WeChat mini-program serves as the platform interface, enabling user interaction with the server-side. The server-side hosts API interfaces and fluorescence detection functions implemented in Python. It facilitates the processing, analysis, and feedback of the uploaded images.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-16-roadmap.jpg" Label="Fig.16 Technical Roadmap"/>

​	In the project introduction page, you will learn about the background and purpose of our project. We will explain to users how we analyze and evaluate the characteristics of the test reagent through fluorescence detection technology. The uploaded images will be transmitted to our server for fluorescence detection processing. In the results display page, we present the processed data and charts in an intuitive and understandable way so that users can view the detailed results of fluorescence detection.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-17-ui.png" Label="Fig.17 Operation Page"/>

​	This project uses Python to write functions for fluorescence detection. The detection process includes: images loading, threshold processing, contour detection, drawing the detection region, measuring fluorescence intensity, curve fitting, plotting, and saving results. The operation process in the WeChat Mini Program is as follows.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-18-instructions.png" Label="Fig.18 Operation Process"/>

# FAQ

## Does the hardware address a need or problem in synthetic biology?

​	Certainly! We have developed a compact, portable, and user-friendly platform for screening and evaluating DNA damaging reagents. This enhances the practicability of the synthetic biology circuits we have constructed. In scenarios requiring real-time detection, our platform enables rapidly and accurately screening and evaluating damaging reagents, significantly reducing dependence on laboratory equipment and specialized personnel. Additionally, our microfluidic chip is designed to automatically generate concentration gradients. This allows a single experiment to test our synthetic biology sensors' response to multiple concentrations of samples, reducing complexity in operations and minimizing reagent waste. The engineering bacteria are stored in a freeze-dried powder form within the chip, greatly facilitating their use. Concentration gradient generation, bacterial cultivation, and productization are common challenges in the field of synthetic biology, and we have designed complementary solutions from the perspective of our own project.

## Did the team conduct user testing and learn from user feedback?

​	Yes! We interviewed Professor Zhang Zhonglian,a well-known expert in the field. We presented our case design to Professor Zhang, who suggested that we should have a side window to replace the liquid bag instead of opening the top of the device, as this would enhance the consistency and accuracy of the optical distance.

​	In addition, we conducted random interviews with some students to gather their feedback on the user experience of our device. They provided us with insights from a user's perspective. We initially used a boss structure to place our pulling structure, but students thought a sliding rail structure might be better. Therefore, we added a sliding rail to the device, making it more convenient for users.

​	For students from other majors, the detection principle and usage process of our device might not be clear. Hence, we added user-guidance pages to our WeChat mini program and recorded a demonstration video of the device to facilitate understanding.



## Did the team demonstrate utility and functionality in their hardware proof of concept?

​	Certainly. The physical prototype of our device can be seen in the introductory demonstration video, and we will showcase it to other teams on-site in Paris. We conducted experiments on the designed microfluidic chip involving bacterial cultivation and damaging reagent response to validate the feasibility of our chip's functionality. Additionally, we have thoroughly verified the device's capabilities in sample injection control, constant temperature control, and fluorescence detection, all of which you can find detailed information on this page.



## Is the documentation of the hardware system sufficient to enable reproduction by other teams?

​	Certainly, We have compiled the structural blueprints and fabrication methods for the chip, as well as structural blueprints and internal component choices for the apparatus. You can contact us on the contribution page to obtain relevant files, thereby assisting other participants in replicating our device. Our chip is machined, and the device is crafted using a common 3D printing process, making it easily replicable for other teams to reference and implement.



# Reference

[1]Guillaume Aubry, Hyun Jee Lee, Hang Lu.Advances in Microfluidics: Technical Innovations and Applications in Diagnostics and Therapeutics[J].Analytical chemistry,2023,Vol.95(1): 444-467 

[2]Junbin Li, Na Wang, Mengyi Xiong, Min Dai, Cheng Xie, Qianqian Wang, Ke Quan, Yibo Zhou, Zhihe Qing.A Reaction-Based Ratiometric Bioluminescent Platform for Point-of-Care and Quantitative Detection Using a Smartphone[J].Analytical chemistry,2023,Vol.95(18): 7142-7149

[3]Mazher Iqbal Mohammed.A lab-on-a-chip that takes the chip out of the lab[J].Nature,2022,Vol.605(7910): 429-430

[4] Luo Y, Zhang X, Li Y, et al. High-glucose 3D INS-1 cell model combined with a microfluidic circular concentration gradient generator for high throughput screening of drugs against type 2 diabetes[J]. RSC advances, 2018, 8(45): 25409-25416.

[5]Mazher Iqbal Mohammed.A lab-on-a-chip that takes the chip out of the lab[J].Nature,2022,Vol.605(7910): 429-430

[6] Coluccio M L, D’Attimo M A, Cristiani C M, et al. A passive microfluidic device for chemotaxis studies[J]. Micromachines, 2019, 10(8): 551.

[7] Shen S, Zhang F, Gao M, et al. Concentration Gradient Constructions Using Inertial Microfluidics for Studying Tumor Cell–Drug Interactions[J]. Micromachines, 2020, 11(5): 493.

 
