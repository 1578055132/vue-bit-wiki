<Cover imgSrc="https://static.igem.wiki/teams/4691/wiki/head-15-safety.png"/>


# Abstract

Before the advancement of **DNA torch**, a discussion on potential safety problems was held among team members. We then further investigated those problems and the existing domestic and foreign policies, ensuring that everything done is under the protect of law and ethics. 

After several iterations, we made a decision to divide the Safety part into **'General Safety'** and **'Advanced Safety'** to ensure the comprehensive, thorough, efficient, and sustained nature of our Safety part.



# General Safety
## Project Design Safety

The reaction process of our project is to construct a cascading amplification circuit based on HRP, improving the SOS sensor. It aims to enhance the output level of the sensor, expand its working range,  and achieve the detection of minute samples using a microfluidic chip.Throughout our experimental journey, our team conducted experiments using BL21, DH5a, and MG1655 competent E. coli strains and pUC19 plasmid vectors.These strains of E. coli are classified under Biosafety Risk Group 1 and have been always handled in an aseptic environment.

According to the WHO Laboratory Biosafety Manual, the risk involved in our experiment is **Level I** (no or very low risk), the same level as where we were working in.

## Lab Safety

​	All laboratories in China must be accredited by the Office of the Ministry of Environment in accordance with the standard LST EN ISO/IEC 17025:2018.To ensure safety, team members must comply with all essential safety protocols before working in different laboratories. Prior to entering the lab, each team member is required to familiarize themselves with the experimental manual beforehand, and to receive training on experimental safety measures.

​	All team members must familiar themselves with the safety features in the lab, such as

1. Fire Extinguisher;

2. First Aid Kit;

3. Biosafety  Cabinet;

4. Flame Retardant Benches;

5. the Nearest Safety Shower;

   <ImageAndLabel4 ImageSrc="https://static.igem.wiki/teams/4691/wiki/safety/safety/safety-3-shower.jpg" Label="Fig.1 the Nearest Safety Shower"/>

​	Wastes after experiments must be properly disposed, following not only following rules,

1. Used pipette tips, gels, eppendorf and so on, should be threw into particular biohazard waste bins;
2. Bacteria solutions and reagents could be disposed only after high-pressure sterilization;
3. Toxic or acidic/basic reagents must be neutralized before disposal;

​	Other rules such as,

1. Pay attention to the disinfection of the test bench and experimental equipment, and the equipment contacting biological materials should be autoclaved;
2. Properly handle and store hazardous and(or) combustible materials;
3. Dress code: lab coats, masks, trousers(shorts are forbidden);
4. Long hair should be tied up;
5. etc.

​	Our experiment team strictly abides by the following experiment manuals or regulations throughout the experiment:

WHO, Laboratory Biosafety Manual（2004）

[https://wenku.baidu.com/view/3f946f5b31687e21af45b307e87101f69e31fba9.html?fr=aladdin664466&ind=1open in new window](https://wenku.baidu.com/view/3f946f5b31687e21af45b307e87101f69e31fba9.html?fr=aladdin664466&ind=1)

Classified Catalogue of Medical Wastes in China (October 10, 2003)

[https://wenku.baidu.com/view/ba9b15f77c1cfad6195fa770.htmlopen in new window](https://wenku.baidu.com/view/ba9b15f77c1cfad6195fa770.html)

Standard for special packaging bags, containers, and warning symbols for medical wastes (November 20, 2003)

[http://www.mee.gov.cn/gkml/zj/wj/200910/t20091022_172239.htmopen in new window](http://www.mee.gov.cn/gkml/zj/wj/200910/t20091022_172239.htm)

Administrative Punishment Measures for Medical Waste Management (2004-05-27)

http://www.mee.gov.cn/gkml/zj/jl/200910/t20091022_171825.htm



# Advanced Safety

## Chip on GMOs

​	In the project, our genetic circuits are designed in E. coli to detect DNA damage effect. Inevitably, we have to use a cell system instead of cell-free system. Thus we've made some measures.

### 	what we did

##### 		On Team Member

1. We elaborated a course on GMOs experiments guided by master students, learning the details of the experiment and some emergency measures facing unpredicted situation;
2.  Read lots of correlative papers, understanding of the principles;

##### 		On Product

1. In Vitro Detection: no worries of being contaminated by GMOs;
2. No Direct Contact: users only need to pour the sample liquid into the sample inlet, and are not suggested to open our devices if not necessary;
3. Packed Chip: chips are packed, thus assuming users don't break it on purpose, users won't have opportunity to contact the reagents inside; 
4. Modular design: if it's expired or not working, users only need to change the particular parts of it , no need to disassemble it;

## Cell System

​	Advantages:

1. Efficient Production: utilization of metabolic activity and biosynthetic capabilities of cell;
2. Self-repair capability: maintaining a stable production process and  product quality;

​	Disadvantages:

1. Control and Stability Challenges: easily influenced by environmental factors, cell state and physiological conditions;
2. Potential Contamination Risks: impurities within cells or microbial contamination from external sources / possibilities of leaking into the environment;

### 	what we did

​	Our hardware can handle these difficulties. The device is designed with modular approach. We offer users a sample inlet, so users hardly has necessity to directly contact the chemical agents and chips. A heating film has been set in our device, providing our E. coli a temperature-stable circumstance to match its optimal working temperature.  In the mean time, to avoid the contamination of our chip(cell system) and the environment we made the following design,

1. [middle layer] Polyether sulfone(PES) filter membrane(pore size = 0.45μm): covered each top of the eight interconnected circular cultivation chambers, preventing the escape of E. coli bacteria into the atmosphere and aerosol contamination, ensuring biosafety;

2. [lower layer] Sealing Ring: aligned with the cultivation chambers, preventing leakage of liquid;

   <ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/safety/safety/safety-4-chip.jpg" Label="Fig.2 the Chip Structure"/>







## Wet Lab Training

​	Not only need the wet-lab group member to accept professional lab training , but also those who in the BIT team and those students who has aspiration to do experiments in lab do so. 

### 	what we did

​	Our former teammates, with professors in the research group, started up an extended course to give everyone attended(members of wet-lab as well as members who want to help are asked to participate) a professional lab training. Surprisingly the course are really successful and welcomed by undergraduate students in BIT(Beijing Institute of Technology),  and are planned to carry out every school year.

​	And we also held a lecture in some parts of which contains wet lab training in RDFZ, a high school in Beijing.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/safety/safety/safety-2-bitedu.jpg" Label="Fig.3 Lecture in BIT"/>

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/safety/safety/safety-1-rdfzedu.jpg" Label="Fig.4 Lecture in RDFZ"/>

## Shelf Life and Storage Conditions of the Chip

​	Most product should have its quality guarantee period to make sure the product reach its optimal performance, especially our chip on cell system. And the storage condition should be noticed to users.

### 	what we did 

​	the research group has already made thorough investigation among the shelf life and storage condition of E.coli. they figured out by papers that freeze-dried might be the best way to preserve, and designed lots of comparing experiment among different agents to find out the best matching protective agent.

​	eventually, our engineering bacterial powder, freeze-dried under the optimal formulation of protectants(a mixture of 5% trehalose, 2.5% sucrose, and 5% glutamate), demonstrated a survival rate of 96.14% after being stored at 4°C for 60 days. After storage for 180 days under the same conditions, the survival rate decreased to 88.87%.

Reference:

Wang W, Yu Y, Li X, et al. Microfluidic chip-based long-term preservation and culture of engineering bacteria for DNA damage evaluation. *Applied Microbiology & Biotechnology*. 2022;106(4):1663-1676. doi:10.1007/s00253-022-11797-2

## Disposal of Wasted Chip

​	Due to the purpose of our project is to detect the DNA damage caused by reagents of different concentration, there should be high possibility that the chip after used be contaminated by toxic reagents. Therefore an appropriate method of wasted chip disposal is necessary.

### 	what we did

​	During experiment, every reagents and chips containing bacteria properly disposed by the latest standard of 'Specifications for fire safety management of laboratories in colleges and universities(JY/T 0616—2023)' and 'Standard for Pollution Control on Hazardous Waste Storage(GB 18597—2023)'. Our sterilization process mainly involves ultraviolet irradiation and alcohol cleaning.

​	And for the disposable chips, we recommend the users to first neutralize the chemical reagents, and throw it in a particular bag after alcohol cleaning. As if users don't have absolute confidence to deal it with themselves, we recommend them to first  pack it up and hand it to us .

regulations we abides:

Specifications for fire safety management of laboratories in colleges and universities(JY/T 0616—2023)

http://www.moe.gov.cn/srcsite/A03/s3013/202307/t20230705_1067360.html

Standard for Pollution Control on Hazardous Waste Storage(GB 18597—2023):

https://www.mee.gov.cn/ywgz/fgbz/bz/bzwb/gthw/wxfwjbffbz/202302/t20230224_1017500.shtml

