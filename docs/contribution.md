<Cover imgSrc="https://static.igem.wiki/teams/4691/wiki/head-14-contribution.png" />

# Abstract

|     **Type**      |                       **Introduction**                       |
| :---------------: | :----------------------------------------------------------: |
|       Part        |          Update experimental data of existing part           |
| Microfluidic chip | Provide methods of designing, simulating and machining process of the micro-fluid chip |
|      Device       | Provide modeling files and size parameters of hardware devices |
|     Software      |        Provide the source code of WeChat mini program        |
|        HP         | Research and document the designing and implementation of human practices of excellent teams from 2017 to 2022 <br> Document different education materials which fit different groups of audience to increase the influence of synthetic biology and iGEM |
|      Design       | Provide editable illustration materials designed by team BIT |

# Parts: Newly Added LOD Data of BBa K3020000

In this year, team BIT is committed to developing an active DNA damage effect evaluation system, the core goal is to develop a highly sensitive microbial sensor, the principle of responding to DNA damage is based on the SOS reaction of bacteria which can activate the activity of RecA protease, initiate downstream transcriptional activity, and express fluorescent proteins. Team BIT in 2019 uploaded part BBa K3020000 which can respond to DNA damage and is what the genetic circuit design of team BIT in 2023 was carried out on the basis of in order to optimize the DNA damage detection performance. In order to visually compare the difference in detection performance before and after optimization, the limit of detection (LOD) was taken as an indicator, so the LOD data of BBa K3020000 was added. 

<ImageAndLabel4 ImageSrc="https://static.igem.wiki/teams/4691/wiki/contribution-16-graph-of-parts.png" Label="Fig.1 LOD data of BBa K3020000"/>

The test is described at:

http://parts.igem.org/Part:BBa_K3020000:Experience

# Hardware: Processing Steps of Micro-fluid Chips

In this section we will share the design, simulation and machining process of statically cultured micro-fluidic chips and external machines in detail, and we will also open up solidworks 3D models of all parts to the public, hoping that other teams can replicate our products or receive some inspiration from them.

## The Micro-fluid Chip

###  ① Designing Process of the Micro-fluid Chip

To meet the needs of incubating bacteria inside the chip, we use a combination of circular mixers and helical coil mixers to achieve a series of concentration gradients of dilution, reducing the volume of the chip while ensuring that the liquid is fully mixed. The chip contains 8 chambers to realize high throughput detection. In order to ensure the cost of the chip and the clarity of the fluorescence shot, we chose to use PMMA (acrylic resin) as the final material.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/contribution-10-chip-design.png" Label="Fig.2 Overall Design of the Micro-fluid Chip"/>

### ② Simulation of the Micro-fluid Chip

In order to verify whether the chip designed can produce concentration gradient and meet our requirements, team BIT simulated the chip structure. The simulation includes importing the three-dimensional chip model we had built in Solidworks into Comsol, designing specific boundary conditions and simulating the fluid motion inside the micro-fluid chip.

The simulation results are shown as follows:

<ImageAndLabel4 ImageSrc="https://static.igem.wiki/teams/4691/wiki/contribution-8-concentration.png" Label="Fig.3 Simulation Results of the Micro-fluid Chip Concentration"/>

<ImageAndLabel4 ImageSrc="https://static.igem.wiki/teams/4691/wiki/contribution-7-flow-velocity.png" Label="Fig.4 Simulation Results of the Micro-fluid Chip Flow Velosity"/>

The results are consistent with expectations. Team BIT provide Comsol documentation as reference for other teams planning to do fluid simulation on micro-fluidic chips.

<PdfEmbeded pdfSrc="https://static.igem.wiki/teams/4691/wiki/contribution-15-comsol-simulation-instruction.pdf" />

### ③ Machining Process of the Micro-fluid Chip

The chip is divided into three layers, the material is PMMA. We use 3D modeling software to draw the chip structure and produce the chip by machining, and then we use thermal adhesive and hot pressing to bond the chip. Team BIT will display the detailed design of the micro-fluid chip so that others can get inspiration from it.

**Contact us through the mail address at bottom to get access to the whole file!**  

The source documents of the drawings are as follows: 

<ImageAndLabel4 ImageSrc="https://static.igem.wiki/teams/4691/wiki/contribution-9-blueprint.png" Label="Fig.5 Drawings of the Micro-fluid Chip"/> 

## The Hardware Device

Our aim is to design a device that can realize fluid injection and control, temperature control, and fluorescence detection. The following is the name and parameter of the key device selected in the process. The description of each module of the whole machine is shown in the figure, and the detailed size and modeling file will be publicly provided for other teams.

**Contact us through the mail address at bottom to get access to the whole file!**  

The following are the related devices we selected during the construction process: 

Sample injection: LEFOO LFP101ADB miniature peristaltic pump 

​	Silicone hose (1.0mm*2.0mm) 

​	Steel needle (outer diameter 1.0mm) 

Temperature control: PT1000 heat sensor 

​	Silicone heating film 

​	TCM1040 small version thermostat 

Fluorescent: LED light source (luminous wavelength: 480-485nm) 

​	PHTODE GNPF520 narrow band filter

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/contribution-6-whole-device.png" Label="Fig.6 Drawings of the Hardware Device"/>

## Software

The overall function of the WeChat mini program of this project is to process pictures of fluorescent and display the test results. After capturing fluorescence images by smartphone or locally, we can obtain the intensity information of fluorescence by uploading them to WeChat mini program we designed for quantitative analysis. 

**Contact us through the mail address at bottom to get access to the source code!**  

# Human Practices: What Would Make a Useful Contribution for Future iGEM Teams

## Grand Research of Human Practices

Before team BIT launched the work of human practices in 2023, in order to better understand the implementation methods and principles of human practice, team members Hanzhi Zhang, Jianing Wang, Kuanlong Lee and Yuhang Wu of team BIT studied and researched the human practices of 40 teams that won the Grand Prize, Runner Ups, Best Village Award and special awards related to human practices and their nominees from 2017 to 2022, summarizing what we should learn from these teams and what need to be paid attention to. We document our ideas in the form of a PDF document as the research report for members of team BIT to know what makes great human practices every year. 

Team BIT will continue this research report in 2024 and beyond projects, with full consideration of intellectual property rights, as a reference to guide our team members in making plans for human practice each year, which we find invaluable and meaningful! In the future, we will also study and research the human practices of the teams that have achieved extraordinary results in 2023 and record the experience as updates, and will stick to it year by year. We believe that this is very helpful for the implementation of human practice, because it gives us ideas worth learning and practices should be avoided. At the same time, we will also fully encourage the creativity of the team members to turn the experience they learned into their own inspirations and implement them as human practice projects of team BIT in 2023!

<PdfEmbeded pdfSrc="https://static.igem.wiki/teams/4691/wiki/contribution-11-hp-grand-research.pdf" />

## Serial Education held by team BIT, RDFZ-CHINA,  BNDS-China together

In 2023, team BIT plans to expand the target audience of synthetic biology lectures and enhance the usability and dissemination of educational materials, building on the education projects in 2022. This requires a larger production team - so we contacted team RDFZ-CHINA and team BNDS-China after summarizing the materials of previous years' education projects of team BIT. We shared the education materials with each other, then selected synthetic biology knowledge which is suitable for junior high school students, high school students, and undergraduates. It was saved as a PowerPoint document and presented to students in all three schools afterwards.







Team BIT, aligning with team RDFZ-CHINA, BNDS-China, jointly held three lecturing courses for junior and senior high school and undergraduate college students to popularize the classical techniques of synthetic biology, to demonstrate the responsibility of synthetic biology researchers to the community and society through biosafety and sustainable development, and to introduce their respective projects. In the process of the activity, it was found that multiple teams carrying out human practice projects could promote further communication among the teams, improve the quality of publicity, expand audiences geographically and numerically, share resources, and study technology and methods mutually, which also promoted the mutual learning and two-way dialogue to a more extensive and in-depth degree. In the future, we will also contact more teams in Beijing to conduct educational projects to expand the influence of iGEM in Beijing and spread it to the world through the Internet. The relevant education materials are shown in the attachment for the reference of other teams.

<ImageAndLabel4 ImageSrc="https://static.igem.wiki/teams/4691/wiki/contribution-3-lecturing-course-with-rdfz-china.png" alt=""  />

<PdfEmbeded pdfSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/contribution-12-education-junior-high.pdf" />

<ImageAndLabel4 ImageSrc="https://static.igem.wiki/teams/4691/wiki/contribution-1-lecturing-course-with-bnds-china.jpg" Label="" />

<PdfEmbeded pdfSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/contribution-13-education-senior-high-pptx.pdf" />

<ImageAndLabel4 ImageSrc="https://static.igem.wiki/teams/4691/wiki/contribution-2-lecturing-course-with-bnds-china-and-rdfz-china.png" Label=""  />

<PdfEmbeded pdfSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/contribution-14-education-undergraduate.pdf" />

# Art Design: Personal Cartoon Image of Team Members 

In this year, Yuxin Xiao, member of team BIT who is in charge of art design, designed several sets of illustration materials according to the personal characteristics of other team members, unified the personal image design style with the visual identification system of team BIT and drew personal image pictures then uploaded them to the Members page, and now the relevant source files of materials are disclosed as follows for other teams' reference.

<ImageAndLabel4 ImageSrc="https://static.igem.wiki/teams/4691/wiki/footer/cartoon-collection.png" Label="Fig.8 Personal Cartoon Image of Team Members"  scale="2"/>

