<Cover imgSrc="https://static.igem.wiki/teams/4691/wiki/head-3-experiment.png" />



# Biological experiment plan

## Strain Obtain

### gene acquisition

​	The recA，rbs30，rbs32，hrpR，hrpS，phrpL and eGFP genes were synthesized by  Sangon Biotech according to the standard of biobrick, and the line fragments were obtained through PCR.

#### PCR

##### Material

​	Thermocycler, (0.2ml)PCR tube, pipetting gun, pipette tip, 2xPCRmix, upstream primer, downstream primer, sterile water, DNA template.

##### Protocol

- Add to PCR tube (the total system volume: 25 μL): 12.5 μL 2xPCRmix, 9 μL of sterile water, 1.5 μl DNA template, 1 μL upstream primer, 1 μL downstream primer;
- Put the PCR tube into the thermocycler and execute the program: pre-denaturation at 94°C for 210s; followed by 30 cycles of denaturation at 94°C for 30s, anneal at the range of 52-62°C for 30s, extension at 72°C for 60s;and a final extension at 72°C for 5 min;
- The product is stored at -10℃;

#### Agarose gel electrophoresis and sample recovery

##### Material

​	Electric hot water bath, electronic balance, electrophoresis instrument, gel imaging analysis system, pipetting gun, pipette tip, glass bottle, agarose, electrophoresis buffer, nucleic acid dye, DNA loading buffer, DNA precipitation buffer, gel buffer, elution buffer;

##### Protocol

​	Agarose gel electrophoresis

- 1% agarose solution: add 50mL of 1X TAE electrophoresis buffer into a glass bottle. then weigh 0.5g of agarose and add it into the bottle; 
- Heat the mixture(1% agarose solution) to boiling. Add 5μL of fluorescent dye to the heated liquid and mix it completely. Take out the glue box and place a mold in the box. Then pour the mixture into the mold and wait for solidification at room temperature;
- Remove the cooled and formed agarose gel and place it in the electrophoresis instrument. Mix the DNA samples with 1/5 of DNA precipitation buffer thoroughly. Then inject the standard samples and the mixture into different holes of the gel. Turn on the electrophoresis instrument and adjust the voltage to 110V  for 35 min;
- Take out the agarose gel and open the gel imaging analysis instrument to analyze and save the electrophoresis results.

​	Sample recovery

​	The electrophoresis products were purified by gel recovery method, and the purified materials were using the DNA gel recovery kit of Axygen. The operation steps are as follows :

- Open the water bath in advance and set the temperature to 75℃(167°F);
- Use Agarose gel electrophoresis to verify the DNA fragment and observe in the gel imaging system. Cut the agarose gel containing the target band with a knife under the ultraviolet lamp, and record the weight of the 1.5 mL centrifuge tube in advance. After the centrifuge tube is loaded, weigh it again and calculate the gel weight. 100 mg is a gel volume equivalent to 100 μL;
-  Add 3 gel volume of gel buffer DE-A, mix well and put into 75°C water bath  heating 6-8 min, mix it up and down every 2 min during water bath;
- Add 0.5 DE-A volume of gel buffer De-B, uniform mix it, and add 1 gel volume of isopropanol;
- Purify the resulting products by centrifugation. Transfer the above solution to an adsorption column and place it in a 2mL centrifuge tube. Centrifuge at 12,000 rpm for 1 min, discard the waste liquid, and place the adsorption column back into the collection tube;
- Add 500 μL buffer W1 to the adsorption column, centrifuge at 12,000 rpm for 30s, pour the waste liquid, and place the adsorption column back into the collection tube;
-  Add 700 μL buffer W2 to the adsorption column, centrifuge at 12,000 rpm for 30s, pour the waste liquid and place the adsorption column back into the collection tube, and repeat the operation once again;
-  Put the adsorption column in a new 1.5ml centrifuge tube, add 30μl Eluent in the center of the adsorption column membrane, and then centrifuge at the 12,000rpm  for 1min after standing at room temperature, from which electrophoretic products were obtained;

###  Plasmid Construction

​	The obtained gene line fragment and pUC19 empty vector are digested and ligated by enzyme ligation.

#### Enzyme digestion

##### Material

​	Incubator, (0.2ml)PCR tube, pipetting gun, pipette tip, restriction enzyme, NEB Buffer 3.1, target gene

##### Protocol

​	After adding the following enzyme digestion system to a 0.2 mL centrifuge tube, incubate it in an incubator at 37°C for 2 h.



<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/ep-1-digestion.jpg" Label="Fig.1 Enzyme Digestion Dystem"/>

#### Enzyme Ligation

##### Material

​	Incubator, (0.2ml) PCR tubes, pipetting gun, pipette tip, T4 DNA ligase Buffer 10x, Nuclease-Free Water, T4 DNA ligase, vector fragment, target fragment;

##### Protocol

​	After adding the following ligation system to a 0.2mL centrifuge tube, incubate it in an incubator at 37°C for 24 h.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/ep-5-ligation-reaction.jpg" Label="Fig.2 Ligation Reaction"/>


### Transformation and Selection

​	Transfer 5μL of the ligation product into 50μL of BL21 competent cells. After incubation, plate 100μL of the transformed cells onto LB agar plates containing 100μg/mL ampicillin resistance. Invert the plates and incubate overnight at 37°C. Pick single colonies and inoculate them into 30mL of antibiotic-free LB broth. Add 30μL of ampicillin and incubate at 37°C with shaking at 180r/min for 12 h. Preserve the cultures in glycerol at -10°C;

#### Plasmid transformation

##### Material

​	Vertical clean bench, shaker, water bath, centrifuge, pipetting gun, pipette tip, solid culture dish, alcohol lamp, applicator, ampicillin, LB-free liquid medium, competent cells;

##### Protocol

- Fill the foam box with about 2/3 of the volume of ice, and take100μL of prepared competent cells. turn on the ultra-clean table UV lamp in advance for 15min for sterilization. After sterilization, light up the alcohol lamp in the ultra-clean table, and  transfer 50μL competent cells into a new aseptic centrifuge tube with a pipette beside the alcohol lamp, and add 5μL plasmid solution into it.
- place the centrifuge tube containing the plasmid solution in the ice box for 30min, turn on the water bath in advance and set the temperature at 42℃, heat the centrifuge tube after the ice bath in the water bath at 42℃ for 45s, and immediately remove the centrifuge tube and place it in the ice box for 2min to complete the transformation process;
- Take out the packaged liquid medium without anti-LB, and add 500μL liquid medium into the centrifuge tube after heating. Incubate the centrifuge tube in a shaking table at 37℃ 180r/min for 1h;
- After removing the centrifuge tube from the shaker, centrifuge it at 7000rpm for 30 s. Carefully aspirate and discard the supernatant (400μL) next to an alcohol lamp. Resuspend the remaining bacterial pellet using a pipetting gun, and based on plasmid resistance, select solid medium containing ampicillin resistance. Using a pipetting gun next to an alcohol lamp, add 100μL of the resuspended bacterial culture onto the surface of the solid medium. Spread evenly using a spreader. Cover the petri dish and invert it, then place it in a 37°C incubator for 12 h;
- Add 30μL of ampicillin to 30mL of antibiotic-free LB liquid medium. Using a pipette tip, pick a single colony and inoculate it into the aforementioned liquid medium. Incubate at 37°C with shaking at 180r/min for 12 h;

## Characterization experiment

### Preparation

​	Transfer The bacteria solution cultured overnight to 30mL fresh non-resistant LB medium, with adding 30μL ampicillin, and culture in the shaking table at 37℃ and 180r/min until the OD600 value of the bacteria solution is between 0.2 and 0.4; 

### Different types of DNA damage

​	Expose the bacterial solution to the UV lamp for the following time: 1 min , 2 min , 3 min, 4 min, 5 min. Then wait it culture in the shaking table at 37℃ and 180r/min. Measure the fluorescence intensity and OD600 every 20min;

### Different types of  DNA damage reagents

​	Add the damaging and non-damaging agents according to the following gradient, and the bacteria solution is reacted in the shaking table at 37℃ and 180r/min for 2 h. Measure OD600 and fluorescence intensity of bacterial solution by microplate reader;

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/ep-2-2-dna-damage-reagent.png" Label="Fig.3 Gradient of DNA-Damaging Reagent and Non-damaging Reagent concentration"/>

## RBS replacement-restructuring experiment

The RBS sequence predicted by the  modeling group is expected to improve the output level of the sensor. RBS upstream of hrpR in the gene circuit was replaced by homologous recombination technology.

<img src=" https://static.igem.wiki/teams/4691/wiki/rbs-replacement.png" alt="Fig.1 RBS Replacement Diagram" style="zoom:50%;" />



### Cloning Reaction Principle

- PCR / enzyme digestion, prepare the linear vector
- Amplifying fragments with primers containing overlapping sequences.
- Mix the carrier, fragment and Basic Assembly Mix  and react at 50 °C for 15 min.
- Plasmid transformation

<img src="https://static.igem.wiki/teams/4691/wiki/cloning-reaction-principle.png" alt="Fig.2 Cloning Reaction Principle" style="zoom:50%;" />

### Primer Design

The original RBS sequence : BBa_B0030  **ATTAAAGAGGAGAAA**

| ID   | Identifier |        Sequence（5’3’）         | Translation Rate（a.u.） |
| ---- | ---------- | :-----------------------------: | ------------------------ |
| 1    | BBa_B0030  | CTAGAG**ATTAAAGAGGAGAAA**TACTAG | 2559.62                  |
| 2    | BBa_B0035  | CTAGAG**ATTAAAGAGGAGAA**TACTAG  | 2897.89                  |
| 3    | BBa_B0064  |  CTAGAG**AAAGAGGGGAAA**TACTAG   | 452.53                   |
| 4    | BBa_B0033  |   CTAGAG**TCACACAGGAC**TACTAG   | 56.59                    |

​                                                                                 Table 1. related sequence

RX-1-X :Original RBS；RX-2-X: Add grey sequence

（Each group of F and R sequences are the same, without repetition. When added, F and R1 are one group, and F2 and R are one group.）



R1-2-F：GAATTCGCGGCCGCTTCTAGAGAGACCTTGTGGCAACAATTT

R1-2-R1：**AGTATTTCTCCTCTTTAATCTCTAG**TTTTACTCCTGTCATGCCGG

R1-2-F1：:**CTAGAGATTAAAGAGGAGAAATACT**AGATGAGTACAGGCATCGATAAGGA

R1-2-R：CTGCAGCGGCCGCTACTAGTTTATTTATACAGCTCATCCATGC



R2-1-R1：**TTCTCCTCTTTAATTTTTACTCCTG**TCATGCCGGGTAATACCGGA

R2-1-F1：**CAGGAGTAAAAATTAAAGAGGAGAA**ATGAGTACAGGCATCGATAAGGACG

R2-2-R1：**TAGTATTCTCCTCTTTAATCTCTAG**TTTTACTCCTGTCATGCCGGGTAAT

R2-2-F1：**CTAGAGATTAAAGAGGAGAATACTA**GATGAGTACAGGCATCGATAAGGAC



R3-1-R1: **TTTCCCCTCTTTTTTTACTCCTGTC**ATGCCGGGTAATACCGGATA

R3-1-F1: **GACAGGAGTAAAAAAAGAGGGGAAA**ATGAGTACAGGCATCGATAAGGACG

R3-2-R1: **CTAGTATTTCCCCTCTTTCTCTAG**TTTTACTCCTGTCATGCCGGGTAATA

R3-2-F1: **CTAGAGAAAGAGGGGAAATACTAG**ATGAGTACAGGCATCGATAAGGACGT



R4-1-R1: **GTCCTGTGTGATTTTACTCCTGTCA**TGCCGGGTAATACCGGATAG

R4-1-F1: **TGACAGGAGTAAAATCACACAGGAC**ATGAGTACAGGCATCGATAAGGA

R4-2-R1: **CTAGTAGTCCTGTGTGACTCTAGTT**TTACTCCTGTCATGCCGGGTAATAC

R4-2-F1: **AACTAGAGTCACACAGGACTACTAG**ATGAGTACAGGCATCGATAAGGACG

#### material

Water bath pot, clean bench, pipette gun, pipette gun head，2×Basic Assembly Mix，Linearized pUC19 Control Vector，Control Insert，Trans1-T1 Phage Resistant Chemically Competent Cell

### protocol

- Vector preparation : The vector template ( pUC19 ) is digested with E enzyme and P enzyme, digested for 2h, and the fragment is recovered by gel.；

- Preparation of target fragment : Design PCR primers

  The forward primer is composed of the forward overlapping sequence of the vector（15-25nt）+Positive specific primer sequence of target fragment（20-25nt）

  Reverse primers are composed of reverse overlapping sequences of vectors.（15-25nt）+Objective fragment reverse specific primer sequence（20-25nt）

- Select the appropriate taq PCR polymerase（pfu），the final concentration of PCR primers is recommended to be 0.2-0.4μM, and the annealing temperature is recommended to be 60-68 °C. The specific PCR procedure is as follows.

  

  <ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/pcr-condition.png" Label="Fig.3 PCR Condition"/>

- Purification of PCR products : If the fragment is derived from the plasmid template and the plasmid has the same resistance as the recombinant vector, the target fragment is digested with DMT enzyme and then purified. The system is 1μL DMT enzyme + 50μL PCR product. Incubate at 37 °C for 60 minutes . Add loading Buffer to a final concentration of 1x, and then electrophoresis.

- Recombine the above products  The total volume of the system is 10μL, the reaction condition is 50 °C for 15 minutes, and then place the centrifuge tube on ice for several seconds.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/ep-establishment-of-cloing-reaction-system.png" Label="Fig.4 Clone Reaction System"/>

- Transformation, add 50 μL of competent cells  with 2 μL of recombinant products, place the tube on ice for 30 minutes, heat shock in a 42 °C water bath for 30 s, and immediately place on ice for 2 minutes. Add 450 μL of medium , put in shaking table at 37 °C for 1 hour, centrifuge at 5000 rpm for 1 minute, and take100 μL of cells for coating and culture.

 

## Data measurement

### Measure the OD600

​	Transfer 200μL of the bacteria solution to a 96-hole black microplate and place it in a microplate reader. Set the wavelength to 600 and measure the absorbance reading.

### Measure the Fluorescence Intensity

​	Transfer 200μL of the bacteria solution to a 96-hole black microplate and place it in a microplate reader. Set the excitation wavelength to 488 nm, emission wavelength to 520 nm, and gain to 75. Measure the fluorescence intensity.



***Each group of data was measured three times and averaged***

## Data processing

### SFU

$$
SFU=\frac{RFU}{OD600}
$$

​	SFU represents specific fluorescence units, RFU represents relative fluorescence units. SFU reflects the fluorescence intensity expressed per unit of bacterial cells, thus avoiding calculation bias in fluorescence intensity caused by differences in bacterial quantities among different samples.

### LOD

$$
LOD=\frac{3\sigma}{S}
$$

​	S is the slope of the linear equation, and σ is the standard deviation of the detection values from 20 blank samples. The formula for calculating the standard deviation is as follows:
$$
\sqrt{\sum(x-\bar x)^2 / (n-1)}
$$

$$
(\bar x) -Sample{\quad}mean{\quad} (n)- Sample{\quad}size.
$$







# Hardware Experiment Program

##  Chip Hardware Performance Test

### Circulation test & Airtightness test

​	The experiment was carried out using red dye. After connecting the instruments, the centrifuge tubes at both inlet tubes were filled with red pigmented water. The power supply was turned on first to exhaust the gas in the device to ensure that the liquid filled the entire device, and then turn off the power supply, the sampling steel needle were inserted into the microfluidic chip. Turn on the power, wait until the red pigmented water fills the entire microfluidic chip, and observe whether there is a clear microchannel pattern and whether there is any leakage.

### Concentration gradient generation test

​	The experiment was carried out using sodium fluorescein and ultrapure water. After connecting the instruments according to the specifications of experiment 1, the centrifuge tubes at the two injection tubes were filled with sodium fluorescein and ultrapure water. Turn on the power supply to exhaust the gas in the device to ensure that the liquid fills the entire device, then turn off the power supply, the injection steel needle were inserted into the microfluidic chip. Turn on the power, after a certain amount of liquid is stored in each chamber, turn off the power and pull out the steel needle, use the pipette to take samples from the air outlet hole by tilting the chip, and tilted the chip and used a pipette to take a sample from the outlet hole, and used an enzyme meter to check the concentration of the liquid within each chamber to see if a concentration gradient was generated.

## Microfluidic chip freeze-drying resuscitation test

### Bacterial Culture Test

1. Each of the eight culture chambers of the chip was supplemented with 240 μL of medium and 60 μL of bacteria solution, and each chamber was sealed with a sealing plate membrane.

2. The chips were incubated in a 37℃ thermostat, and every 30 min, 200 μl of culture solution from one incubation chamber was taken for OD value detection with an enzyme marker for a total of eight measurements. Record the growth time and OD value data.

### Bacterial Resuscitation Test

1. Take the bacterial solution that has been cultured overnight and diluted, test the OD value and record it. Then add 300μL of the engineered bacterial solution into the four chambers of the chip, and put it into the 37℃ thermostat for 2 h. The bacterial solution in the conical flask was incubated in a shaker for 2h.

2. Four chambers of bacterial fluid were taken from 200 μl of the chip and their OD values were measured with an enzyme marker.

### Bacterial Expression Test

1. Take 200 μL of the bacterial solution that has been cultured overnight and diluted to test its OD value, and added 300 μL of the bacterial solution to each of the eight chambers of the chip after the OD value reached approximately 0.3.

2. Add NA inducer to the chamber in a concentration gradient of 0 μM, 2.5 μM, 5 μM, 7.5 μM, 10 μM gradient with two sets of parallel experiments at 2.5 μM, 5 μM, and 7.5 μM concentrations.

3. The chip was placed in a 37℃   for 2h, and the OD value and fluorescence intensity of the bacterial fluid in each chamber were detected using an enzyme marker.

### Bacterial Freeze-drying Resuscitation Experiment

1. E. coli with an OD of 0.3 was made into a lyophilized powder (production process attached).

2. Add 300 μL of culture medium to one portion of lyophilized powder and redissolve a total of eight portions of lyophilized powder. Detect its OD value and transfer it to the chip.

3. Setting up a control group: lyophilized powder and 300 μL of culture medium were added to a 1.5 mL centrifuge tube to re-dissolve it, and detected its OD value.

4. Add NA inducer to the chip chamber and centrifuge tube in a concentration gradient of 0 μM, 2.5 μM, 5 μM, 7.5 μM, and 10 μM gradient, with two sets of parallel experiments at 2.5 μM, 5 μM, and 7.5 μM concentrations.

5. The chips and centrifuge tubes were placed in a 37°C thermostat for 2 h. The OD value and fluorescence intensity of the bacterial fluid in each chamber were detected using an enzyme marker.



## Hardware testing

Inlet Model

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/ep-4-hardware-injection.png" Label="Fig.4 The Schematic Diagram of the Fluid Drive "/>


### Flow Rate Testing

​	First, assemble silicone tubing of the same length with a peristaltic pump, ensuring that the flow path and outlet pressure are consistent by sticking them together at necessary points. Use four centrifuge tubes, fill two of them with ultrapure water, place them side by side behind the workstation and secure them. Insert the sample tubes of the peristaltic pump into the two centrifuge tubes respectively, ensuring they are inserted to the same depth to maintain consistent sample pressure.

​	Weigh the mass of each empty centrifuge tube and record it. Fix them in front of the workstation. After connecting the positive and negative wires of the peristaltic pump, connect the laboratory power supply. Turn on the power to purge the air from the device and ensure that the liquid fills the entire system. Disconnect the power and insert the sample tube connected to the peristaltic pump into the empty centrifuge tube to the same depth. Ensure that both setups run in parallel throughout the process. 

​	When the power is turned on again, start the stopwatch on the phone simultaneously. Collect a certain amount of liquid and then turn off the power while stopping the stopwatch. Weigh the centrifuge tube again, calculate the flow rate of the peristaltic pump using the mass difference and time, and record the current voltage value. Repeat the process until you find the voltage value V that achieves the desired flow rate.

### Temperature control module testing



<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-6-tempcontraldesign.png" Label="Fig.5 Constant Temperature Control Design "/>

​                       

#### Uniformity Testing

​	Randomly select six positions on the silicone rubber heating film and use a Fluke 51Ⅱ thermometer to measure the temperature at each position. If the temperature readings at the six positions are close to the sensor readings, and the temperatures at the six positions are relatively similar to each other, it can be concluded that the silicone rubber heating film exhibits good uniform heating characteristics.

#### Accuracy Testing

​	When the temperature reaches 37 degrees Celsius, maintain the temperature for 10 seconds and then start a 60-second timer. Record the temperature every 10 seconds. The absolute difference between the average temperature and the set temperature should not exceed 0.1 degrees Celsius. If this criterion is met, it can be concluded that the incubation device exhibits good temperature control accuracy.

#### Temperature Control Precision Testing

​	After reaching the steady-state temperature following the heating process, in a continuous power supply condition, the temperature should be maintained around 37 degrees Celsius for an extended period of time. Record the highest and lowest temperatures observed during this period. If the difference between the two temperatures, divided by two, is within 0.2 degrees Celsius of the set temperature, it can be concluded that the incubation device exhibits good temperature control precision.

### Fluorescence detection module testing

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/ep-3-flu-detection.png" Label="Fig.6 Optical Path Design"/>

​	The configuration concentration gradient is 0,2.5,5,7.5, 10 μm fluorescent sodium sodium solution, add different concentration of fluorescent sodium sodium solution to the chip cavity chamber, and obtains the same concentration of fluorescent sodium solution detection images in the fluorescent detection module. repeat three times. Use ImageJ to analyze the same concentration of fluorescent sodium solution. After obtaining the average of the fluorescent strength, draw the fluorescent strength curve to observe whether the curve is linear.



1. Freeze-drying powder preparation:
   1. Take 8 mL of bacterial solution with an optical density (OD) of approximately 0.3 and transfer it to a 15 mL centrifuge tube. Centrifuge at a speed of 3500 rpm for 10 minutes.
   2. Discard the supernatant after centrifugation and add 8 mL of protective agent. Mix well.
   3. Seal the solution in a 24-well plate, adding 300 μL of the solution to each well. Cover the plate with a sealing film, leaving a vent hole above each well using a syringe.
   4. Place the sealed 24-well plate in a -80°C freezer and freeze for 12 hours.
   5. After 1 hour of preliminary freezing in the freeze-drying machine, transfer the 24-well plate into the freeze-dryer and freeze for an additional 12 hours.