
<Cover imgSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/ihpfengmian.png"/>

The core of human practice is to obtain feedback from society and stakeholders and incorporate that feedback into the workflow. This year, we have employed a cyclical iterative approach to accomplish comprehensive human practice. The aim is to ensure that projects align with actual needs, continuously enhance project performance through feedback, meet the requirements of stakeholders, and contribute to society.

<IHPCircle />

# Inspiration

​	As a kind of biomacromolecule, DNA carries the genetic information of an organism, and complete and correct DNA is essential for the development, growth and activity of an organism. The results show that DNA damage caused by environmental factors is an important mechanism for damaging biological tissues and organs, and is closely related to the occurrence of cell carcinogenesis. However, through questionnaire surveys, we find that most people have little understanding of DNA damage, indicating that a large number of people will unknowingly come into contact with products that cause DNA damage (such as some chemical agents), or be in special environments with high incidence of DNA damage (chemical plants, radar stations, etc.). How will the BIT team respond to this situation?

# Idea

## Brainstorming

​	With the inspiration in mind, we came up with the idea of detecting DNA damage to provide warning signs to people. We quickly investigated current DNA damage detection methods and found that current detection methods such as single-cell gel electrophoresis and radioisotope labeling could not meet both performance requirements and application needs. Therefore, we focus on the development of sensitive and accurate DNA damage detection methods, hoping to help scientists carry out DNA damage and repair mechanism research, and help staff in special places evaluate the risk of DNA damage and prevent and deal with it in time.

## Project significance research

​	Before formulating a project, we need to determine the value of the project and whether it will be beneficial to the world.
​	Since some organic matter present in biological and chemical reagents is very easy to induce DNA damage, we contact two groups of experts and practitioners: Dr. Wu Xiaoming, who has more than 10 years of experience in chemical research and development, and Peng Zhao and Wu Minjie, biochemical product developers, hoping that they would evaluate the significance of our project from a professional perspective.

​	During the discussion, all three of them affirm our project. Dr. Wu says that the industry is very concerned about DNA damage, which has become a hot spot in the past 5 or 6 years of drug marketing. Therefore, he believes that our project will be beneficial to society, especially in the pharmaceutical industry. Two practitioners says that groups like them, who are exposed to biochemical agents frequently, want such equipment to help them keep themselves safe.

## Scheme

​	It goes without saying that our DNA damage detection projects not only need to meet high performance requirements, but also be used in multiple scenarios. We preliminarily selected the SOS response-sensitive promoter RecA to respond to DNA damage, introduced the Hrp system into the DNA damage response circuit to improve the transcription signal, and combined with the damage effect accumulation characteristics of microbial sensors to achieve highly sensitive DNA damage effect detection. In terms of hardware, we intended to use the advantages of microfluidic chips to design a microfluidic chip platform made of PMMA to complete the recovery, cultivation and detection process of microbial sensors in an integrated and automated manner. And combined with smart phone applet, real-time detection and analysis could be realized.

# Consultation

​	After completing the first version of the project design, we need to ensure that all parts of the project meet the needs of stakeholders. Therefore, we consult and discuss with various experts in order to continuously improve and update our projects to better meet the needs of the public.

## Background

​	After formulating the project plan, we contacted Dr. Wu Xiaoming again and asked him to evaluate whether our project could meet the needs of society from the perspective of users.

​	Dr. Wu raised the need for testing for DNA damage after accumulation. This helps us a lot, and we introduce an additional concentration-gradient generator into the hardware module to automatically dilute the sample concentration to achieve gradient concentration detection. In addition, he said that the industry divides toxic compounds that may cause DNA damage into five grades, according to which Dr. Wu hopes that we can later explore the DNA damage grading method for a certain compound, and develop a scale to measure and to make a judgment.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/wuboshi1.png" Label="Fig.1 Interview with Dr.Wu"/>

## Biology

​	In terms of biology, we consulted with Professor Zhang Jinfeng of Beijing Institute of Technology and Wu Minjie, biochemical product developer.

​	After understanding our project intention and biological route, Professor Zhang pointed out that if we consider the commercial value of the project, the detection time is also a factor that cannot be ignored while we pay attention to the accuracy and detection limit. And only by doing both of these sectors well can our project be competitive. Based on this, we compared the two amplification lines designed at the beginning of the project in detail, and selected the hrp amplification line with a more significant amplification effect after experiments, so as to shorten the detection time as much as possible.

​	Wu Minjie suggested that when evaluating the accuracy and sensitivity of the project, we can try to use other methods to obtain the actual amount of DNA damage caused by the analyte, and then compare it with the signal obtained by the whole cell biosensor, and even perform a conversion to form a standard signal.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/xuejie.jpg" Label="Fig.2 Interview with Wu Minjie"/>

## Hardware

The hardware part of the project involves 3D printing, fluorescence detection and other content. We want to hear what experts with expertise in this area have to say about our project and seek some advice. Prior to this, we made a testing instrument using a boss push-pull structure. We had a discussion with Professor Zhonglian Zhang of Beijing Institute of Technology.

​	Professor Zhang first praised our hardware work, and affirmed the three advantages of our hardware using smartphones: portability, flexibility and low cost-effectiveness. In addition, Professor Zhang also made valuable suggestions: in terms of details, we should improve the internal design of the cassette, such as improving the boss structure that is inferior to the slide rail in terms of accuracy; Make the placement of power supply, temperature control and other parts better. In addition, Professor Zhang believed that we should leave an opening on the side to replace the reservoir instead of placing the cover of the phone from the top to open, which would help improve the consistency and accuracy of optical distance. Professor Zhang also suggested that we do more testing in terms of safety and environmental protection. In terms of application, Professor Zhang believes that the potential users of our products include people exposed to hazardous substances such as chemicals and pesticides, and they need intuitive, portable and easy-to-use testing equipment. The portable test allows them to do the initial test themselves, and then take preventive measures based on the test results, while also increasing their safety awareness.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/yingjian2.jpg" Label="Fig.3 Communicate"/>

​	The exchange with Professor Zhang had let us gain a lot. We listened and added slides to the unit to make it easier for users to operate. In response to the potential user needs raised by Professor Zhang, we developed additional interviews to investigate the students' feelings about the use of our equipment. They gave us some feedback from the user's point of view. For example, for non-professionals, it may not be clear about our detection principle and use process. Therefore, we added a user-guided page to the WeChat Mini Program and recorded a demonstration video of the physical operation.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/yingjian.jpg" Label="Fig.4 Interview with Dr.Zhang"/>

​	In addition, we also invited biochemical product developer Peng Zhao to give suggestions from the perspective of users. He put forward three aspects: First, the issue of safety: worried about the leakage of E. coli, will E. coli have a bad impact on my body or environment? Therefore, we pay attention to packaging in terms of hardware to ensure air-tightness and prevent leakage. The second is the issue of ease of use: he wants simple operations, such as automatic sample filling, detection, and output results. Therefore, we designed simple interactions and used WeChat mini programs to achieve real-time result analysis. The third is the functional problem: he hopes to achieve real-time detection, that is, the signal is constantly updated in real time with the risk of DNA damage in the environment, which can facilitate users to make timely protective measures when they find that the signal exceeds the standard. This is the direction of our future efforts.

# Result

​	This year, we continue to engage with experts, practitioners, professors, and other groups to ask for help and advice, and complete the integrated human practice in the most beneficial way for the project to be carried out. Our project, updated and iterated with the advice and help of all parties, is not only powerful, but also has clear meaning and has certain commercial potential.