<Cover imgSrc="https://static.igem.wiki/teams/4691/wiki/head-5-description.png" />

# Overview

​	**DNA damage** is a phenomenon in which the **DNA sequence undergoes changes that lead to alterations in genetic characteristics**. The causes of DNA damage can be divided into **exogenous environmental factors** (such as chemicals, ionizing radiation, and virus infection) and **endogenous environmental factors** (such as reactive oxygen species). 

​	Research has shown that DNA damage caused by environmental factors can lead to irreversible damage in the body and is closely related to the occurrence of **cell aging** and **carcinogenesis**. Therefore, the development of sensitive and accurate methods for DNA damage detection can **not only** help scientists **investigate DNA damage and repair mechanisms but also** assist workers in specific fields (such as chemical factories and radar stations) **in evaluating DNA damage risk and taking timely prevention and treatment measures.** 

​	DNA damage effects include strand breaks and glycosidic bond cleavage, etc. **Single-cell gel electrophoresis** and **PCR** can distinguish different DNA molecular weights to identify DNA damage phenomena. These methods are **simple and have high sensitivity**, **but** they can only characterize **strand breakage** and **cannot detect other DNA damage effects**. Using **radioactive isotopes** to label specific structures of DNA can monitor DNA damage levels, but this method requires a **professional operating environment** and **usage environment restrictions**. **The combination of chromatography and mass spectrometry** can analyze DNA nucleotide damage levels and damage products with high accuracy, but the method is **costly**, **operationally complex**, and has **limited usage scenarios**. 

​	Therefore, it is crucial to develop an evaluation method for DNA damage effects that meets performance requirements and application needs.

# Problem

​	The existing DNA damage effect evaluation methods usually target a certain type of DNA damage, which cannot meet the actual needs in terms of performance and practicality, and cannot accurately characterize all DNA damage effects. More importantly, these methods are passive monitoring after damage, lacking an active response mechanism, and cannot effectively evaluate DNA damage effects.

# Inspiration

## Biology

​	Team BIT constructed a microbial sensor based on SOS response in 2019, demonstrating that the promoter can respond to DNA damage and activate the expression of downstream fluorescent proteins. At the same time, it was found that the microbial sensor can actively accumulate and respond to [DNA damage effects](https://2019.igem.org/Team:BIT/Project/Background). In order to further improve detection sensitivity, research has been conducted on gene signal amplification circuits. In 2012, Baojun Wang and colleagues used the Hrp (Hypersensitive response and Pathogenicity) gene regulatory system from the Gram-negative bacterium Pseudomonas syringae to achieve high gain transcription amplification of downstream products, and applied it to the highly sensitive detection of arsenic ions. Based on the concept of synthetic biology, the Hrp system can be introduced to control the expression of downstream proteins using DNA damage response promoters, achieving effective amplification of detection signals. By combining the ability of microorganisms to accumulate damage signals, high-sensitivity detection of DNA damage effects can be achieved.

## Hardware

​	Microfluidic chips are the main platform for the implementation of microfluidic technology, which can integrate sample preparation, reaction, detection and other processes of biological, chemical, and medical analysis into a microscale platform. Microfluidic chips have the characteristics of being lightweight, highly integrated, fast reacting, and highly automated. Integrating the constructed microbial sensor with microfluidic chips can achieve integrated and automated DNA damage detection processes. By using small devices to provide the required reaction conditions and utilizing smartphones that are available to almost everyone as detection devices, portable DNA damage detection can be achieved in different scenarios to meet current application needs.

# Solution

## Biology

​	The BIT team chose the SOS response sensitive promoter RecA to respond to DNA damage. Based on the concept of synthetic biology and [the recombinant plasmid construction method](http://parts.igem.org/Help:Standards/Assembly), they referred to existing [Parts: BBa_K2967008](http://parts.igem.org/Part:BBa_K2967008) and [BBa_K2967011](http://parts.igem.org/Part:BBa_K2967011), and introduced the Hrp system into the DNA damage response circuit, which greatly increased the transcriptional signal. By combining the damage effect accumulation characteristics of microbial sensors, high-sensitivity DNA damage effect detection was achieved.

## Hardware

​	Based on the advantages of microfluidic chips, a PMMA material microfluidic chip platform was designed to achieve integrated and automated microbial sensor recovery, cultivation, and detection processes. Additionally, a concentration gradient generator module was introduced to automatically dilute sample concentrations. A self-designed small device was designed to have temperature control, fluid control, and optical detection capabilities. By using a smartphone for detection and analysis of fluorescence signals, the limitations of the scene were overcome, and multi-scene, portable DNA damage detection was completed. An active DNA damage evaluation system was constructed.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/discription/discription/description-solution.jpg" Label="Fig.1 Solution"/>



References

[1]    Huang R X, Zhou P K. DNA damage repair: Historical perspectives, mechanistic pathways and clinical translation for targeted cancer therapy [J]. Signal Transduction and Targeted Therapy, 2021, 6(1): 254-289.

[2]    Wojewódzka M, Buraczewska I, Kruszewski M. A modified neutral comet assay:: Elimination of lysis at high temperature and validation of the assay with anti-single-stranded DNA antibody [J]. Mutation Research-Genetic Toxicology and Environmental Mutagenesis, 2002, 518(1): 9-20.

[3]    Trang P T K, Berg M, Viet P H, et al. Bacterial bioassay for rapid and accurate analysis of arsenic in highly variable groundwater samples [J]. Environmental Science & Technology, 2005, 39(19): 7625-7630.

[4]    Wang B J, Barahona M, Buck M. Engineering modular and tunable genetic amplifiers for scaling transcriptional signals in cascaded gene networks [J]. Nucleic Acids Research, 2014, 42(14): 9484-9492.

[5]    Shen S F, Zhang F J, Gao M Q, et al. Concentration gradient constructions using inertial microfluidics for studying tumor cell-drug interactions [J]. Micromachines, 2020, 11(5):493-506

[6]  http://parts.igem.org/Part:BBa_K2967008

[7]  http://parts.igem.org/Part:BBa_K2967011