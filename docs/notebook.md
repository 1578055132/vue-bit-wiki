<Cover imgSrc="https://static.igem.wiki/teams/4691/wiki/head-8-notebook.png"/>


# January

## Review and Summary

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-1-mindmap.png" Label="Fig.1 Mind map"/>

# February

A Winter Camp was held, to popularize Synthetic Biology and select team BIT's members.

After several weeks, Team BIT was founded.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-2-imageseries1.png" Label="Fig.2 Winter Camp and Team building"/>

# March

We received further information and fundamental lessons of iGEM.

Every member of the team identified their roles and tasks.(wet lab, model, hardware, wiki and etc.)

A meeting among team members was arranged to plan the follow-up works for each section.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-3-meeting.png" Label="Fig.3 Meeting in March"/>

# April and May

**Project-research Group** 

​	Under the guidance of the guidance group, literature review and learning were carried out.

**We team**

​	Investigated the shining parts and learned the project of awarded teams.

**Model**

​	We invited Mr.wang, the headquarter of a 2022 iGEM gold team，to conduct modeling training together with FAFU and HainanU.

**Hardware**

​	Started learning solidworks, comsol, WeChat mini-program, researching microfluidic chip, making research and study of previous projects to get inspiration.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-4-documents.png" Label="Fig.4 Records of learning outcomes" scale="1.5"/>


# June and July

We polished our project and decided our final topic.

All student members learned laboratory fundamental qualities and received basic experimental training.

We divided the work into 5 parts, and let group of each part to promote different aspects of work.

**We team**

​	 Learned about project requirements and gained feedbacks in various ways. Prepared various publicity and education materials

**Wet Lab**

​	Built a gene circuit, and received advanced experimental operation skills.

**Model**

​	 It was determined to guide the experiment and construct the model from three perspectives: promoter, rbs and E.coli growth. Learned how to use the relevant tools.

**Hardware**

​	began to design microfluidic chip, use comsol software to simulate the chip concentration gradient  generator and design WeChat mini-program.

**Art**

​	Started designing Team Logo.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-5-imageseries2.png" Label="Fig.5 Records of learning outcomes"/>



# August

**Wet Lab**
    Carried out strain construction and preliminary characterization.

**Hardware**
    Many versions of microfluidic chips were designed and the final style of microfluidic chips was decided. Meanwhile the simulation of the concentration gradient generator and WeChat mini-program were finished.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-6-someresults.png" Label=""/>


<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-7-someresults.png" Label=""/>


<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-8-someresults.png" Label="Fig.6 Records of some results"/>



# September

We've got our team costumes！

**Wet Lab**

​	Conducted sensitivity testing of the sensor and characterization testing of different types of damage.

**Hardware**

​	Microfluidic chip production, microfluidic chip related devices design and production, experimental testing.

**Model** 

​	further works in three aspects  :

​		Promoter: has made further investigation on the selecting tools of promoter, then made an intensity prediction on a promoter-intensity prediction website and program, but came up with bad results;

​		rbs: has made further investigation on the selecting tools of rbs, tested on some prediction websites, and decided to ue abs calculator as actual-use tool;

​		Protein: has made further investigation on simulation of protein docking, using the method to simulate the formation of HRPs and the binding of HRPs to delta54.

​	Has made further research on the growth curve of E.coli and aiming to integrate it with biological experiments. We intended to carry out the modeling of sensors and amplifiers, and engineer the entire sensing and amplification circuit.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-9-someresults.png" Label=""/>

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-10-somedesigns.png" Label=""/>

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-11-somedesigns.png" Label="Fig.7 Records of some results"/>



# October

**Wet Lab**

​	characterized the sensor's response to different damaging reagents.

**Hardware** 

​	Tested the performance of the chip, constant temperature control module, and fluorescence detecting module, and made some relative wet lab experiments on the microfluidic chip together with Wet Lab group. 

**Wiki** 

​	wrote wiki.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-12-someresults.png" Label=""/>

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-13-someresults.png" Label=""/>

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/notebook/notebook-14-someresults.png" Label="Fig.8 Records of some results"/>

