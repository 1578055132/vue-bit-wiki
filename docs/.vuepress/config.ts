import { defaultTheme, defineUserConfig } from "vuepress"
import { registerComponentsPlugin } from '@vuepress/plugin-register-components'
import { path } from '@vuepress/utils'
import mathjax3 from 'markdown-it-mathjax3'

export default defineUserConfig({
    base: "/bit/",
    theme: defaultTheme({
        navbar: [
            // NavbarItem
            {
                text: 'Foo',
                link: '/foo/',
            },
            // NavbarGroup
            {
                text: 'Group',
                children: ['/group/foo.md', '/group/bar.md'],
            },
            // 字符串 - 页面文件路径
            '/bar/README.md'
        ],
    }),
    head: [['link', { rel: 'icon', href: 'https://static.igem.wiki/teams/4691/wiki/home/logo.png' }]],
    plugins: [
        registerComponentsPlugin({
            componentsDir: path.resolve(__dirname, "./components")
        }),
    ],
    alias: {
        '@theme/Navbar.vue': path.resolve(__dirname, './components/Navbar.vue'),
        '@theme/Sidebar.vue': path.resolve(__dirname, './components/Sidebar.vue')
    },
    extendsMarkdown: md => {
        md.use(mathjax3);
        md.linkify.set({ fuzzyEmail: false });
    }
})