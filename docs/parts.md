<Cover imgSrc="https://static.igem.wiki/teams/4691/wiki/part-background.png" />

BIT registered 4 new basic parts and 1 new composite part .

The sequence source and construction method of the composite part are demonstrated in the Experiments section and Results section. The type of the 4 basic parts belongs to the RBS, as detailed in the Model section. Please click on the Part Name to shift to the registry for viewing further information.

# Basic part

| Part Name      | Type | Sequence（5’3’）            | Translation Rate |
| -------------- | ---- | --------------------------- | ---------------- |
| [BBa\_K4691001](http://parts.igem.org/Part:BBa_K4691001) | RBS  | CTAGAGATTAAAGAGGAGAAATACTAG | 2559.62          |
| [BBa\_K4691002](http://parts.igem.org/Part:BBa_K4691002)| RBS  | CTAGAGATTAAAGAGGAGAATACTAG  | 2897.89          |
| [BBa\_K4691003](http://parts.igem.org/Part:BBa_K4691003) | RBS  | CTAGAGAAAGAGGGGAAATACTAG    | 452.53           |
| [BBa\_K4691004](http://parts.igem.org/Part:BBa_K4691004) | RBS  | CTAGAGTCACACAGGACTACTAG     | 56.59            |

#  Composite part

[**BBa_K4691000**](http://parts.igem.org/Part:BBa_K4691000)

The composite part is composed of 7 basic parts, which can realize the detection of oxidative stress damage level and signal amplification function of E.coli.

Our parts successfully detected the DNA damage caused by the damaging reagent NA , and increasing the signal output while improving the sensitivity of the sensor, which broadens the application range of the sensor and is more suitable for the detection of new chemical reagents.

Reference parts:

| Part Name    |    Type    |                         Description                          | Length |
| ------------ | :--------: | :----------------------------------------------------------: | :----: |
| BBa_K629001  | Regulatory | This part could be started with exposure to irradiation, UV, nalidixic acid and activate SOS system-- DNA repair. | 124bp  |
| BBa_B0030    |    RBS     |                    Modified from R. Weiss                    |  15bp  |
| BBa_B0032    |    RBS     |                    Derivative of BBa_0030                    |  13bp  |
| BBa_K2967008 |   Coding   |  The activator protein HrpR in hrp gene regulatory network   | 948 bp |
| BBa_K2967009 |   Coding   |  The activator protein HrpS in hrp gene regulatory network   | 912pb  |
| BBa_K2967011 | Regulatory |    The inducible promoter in hrp gene regulatory network     | 208 bp |
| BBa_E0040    |   Coding   | Green fluorescent protein derived from jellyfish Aequeora victoria wild-type GFP (SwissProt: P42212) | 720bp  |
| BBa_B0015    | Terminator |               double terminator (B0010-B0012)                | 129bp  |