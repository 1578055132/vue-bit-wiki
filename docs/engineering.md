<Cover imgSrc="https://static.igem.wiki/teams/4691/wiki/head-4-engineering-success.png" />

# Overview

​	The goal of team BIT is to build a highly sensitive biosensor which can detect DNA damage effect. We also plan to design micro-fluidic chip and miniaturized device as reaction platform to meet the needs of application. Therefore, we have two modules, **Biosensor and Hardware**, which are followed the DBTL principle and engineering design cycle.

# Biosensor

##  **Initial Inspiration and Aim**

​	To build the Biosensor that can achieve our desired goal, we have investigated relevant thesis about genetic circuit improvement and bio-amplifiers. Baojun Wang reported the Hrp (hypersensitive response and pathogenicity) gene regulatory network from Pseudomonas *syringae*, which can be used as amplifier in E._coli_ and is capable of amplifying a transcriptional signal with wide tunable-gain control in cascaded gene networks.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/engineering-success/engineering-0-the-architecture-of-the-hrp-genetic-amplifier.png" Label="Fig.1 The architecture of the Hrp genetic amplifier"/>

​	In this year, team BIT has spent great efforts in biological engineering. Here we present our composite part that have been proved to work as desired through our engineering process, respectively [BBa_K4691000](https://2023.igem.wiki/bit/part). Please visit our results page to find more about our [engineering](https://2023.igem.wiki/bit/results).

​	The Hrp system was introduced into the DNA damage response circuit which is a composite part that contains *RceA*, *Hrp*, and *egfp* and is designed to have the ideal E._coli_ which is capable of specifically sensing DNA damage and amplifying a transcriptional signal in cascaded gene networks.

​	**Engineering process is mainly about** verifying the composite part that can amplify a transcriptional signal and enable highly sensitive detection of DNA damage. That's why we chose to build them.

## **Design**

​	We respectively inserted P*RecA* hrpR, hrpS, P*hrpL*, egfp and their composition fragments into the pUC19 plasmid, then introduced the plasmid into expressing Escherichia coli BL21. It is hoped to verify that the composite plasmid introduced with hrp system has a lower LOD (limit of detection) than the plasmid without the amplification system, which is conducive to the highly sensitive detection of DNA damage effect.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/biosensor-circuit.png" Label="Fig.2 The architecture of the DNA damage response circuit with Hrp genetic amplifier"/>


​	We selected different concentrations of NA (Nalidixic Acid) to induce DNA damage. The blank group did not add NA, and the negative control group selected Escherichia coli that was transferred to PUC19 plasmid without hrp system.

## **Build**

​	Genetic circuit: Gene recA, rbs30, rbs32, hrpR, hrpS, phrpL and eGFP were all synthesized by Sangon Biotech according to BIOBRICK standard, and the gene circuit was obtained by PCR and verified by agarose gel electrophoresis.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-1.png" Label="Fig.3 Verification results of gene circuit by agarose gel electrophoresis"/>


​	Construction of plasmid: The gene circuit was connected to pUC19 vector by enzyme link.

​	Ideal strain: The constructed plasmid was transferred into BL21 receptor cells and coated on LB solid medium containing ampicillin.


## **Tests**

### Test 1: Functional Characterization of the Amplifier

​	HRP and wild bacteria cultured overnight were transferred to 30mL fresh medium, and cultured with 30μL ampicillin for logarithmic growth. Then, the bacteria were induced under 1μM NA (Nalidixic Acid, which can inhibit the synthesis of deoxyribose algorithm)  and placed in a shaker at 180r/min at 37℃. OD value and fluorescence intensity were measured every 20 minutes.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-6.png" Label="Fig.4 RFU Over Time for both Strains"/>

​	The RFU of the two strains increased with time. The basic RFU of the HRP strain was higher than that of the wild strain and had a faster growth rate. After 600min, the RFU of the HRP strain reached 4 times that of the wild strain. The results showed that the HRP amplifier was successfully constructed, and the addition of HRP amplifier could significantly improve the fluorescence expression level of E._coli_.

### Test 2: Specific Characterization of DNA Damage Sensors

​	Hydrogen peroxide (H2O2), is commonly used in sterilization, can cause damage to bacterial DNA; Acetone, a less toxic chemical that precipitates proteins; KANA, an antibiotic commonly used in laboratories, inhibits bacterial protein synthesis. HRP strains were induced under different concentrations of NA, H2O2, acetone and KANA each for 2 hours.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-9.png" Label="Fig.5 SFU of SensorResponse to Damaged and Non-damaged Reagents"/>

The results showed that the strain had a good response to DNA damaging agents such as NA and H2O2, while the non-damaging agents such as acetone and KANA did not cause the response of the strain. The experimental results demonstrate that our sensor can respond to different DNA damaging agents and can successfully distinguish between damaging agents and non-damaging agents.

### Test 3: Sensitivity Test of the Amplifier

The overnight cultured HRP bacteria were transferred to 30mL fresh medium and cultured with 30μL ampicillin for logarithmic growth phase. The NA induction curve of HRP bacteria was obtained by inducing NA at different concentrations for 2h.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-8-2.png" Label="Fig.6 Sensitivity of the Sensor"/>



The detection limit of HRP sensor detecting NA is 0.01989μM, which is 1.8 times higher than that of wild sensor.

## **Learn**

In conclusion, the composite plasmid introduced with Hrp system has a lower LOD than the plasmid without the amplification system, which is conducive to the highly sensitive detection of DNA damage effect.

So the result is considered to be succeeded in this iteration and got the engineered bacteria we needed for our goal.

# Micro-fluidic Chip

## **Aim**   

- Cultivate strains on the chip

- Generate a series of concentration gradient

- Generate parallel experiments at the same time

## **Design**

Through the study of relevant literature, we first designed a circular hybrid structure of PDMS material. Five different concentrations can be produced and parallel experiments are formed for comparison. However, most PDMS-based chips use plasma to bond, which makes it difficult to bond engineering bacteria to the chip in advance. So in the second version, we gave up the PDMS material and replaced it with a more flexible PMMA material. The chip uses a three-layer structure design. The first two layers are bonded by thermal bonding, and then the screw and the third layer are bonded together. The rubber ring structure is added between the second layer and the third layer to prevent leakage, so that the detachable function can be realized, and then the bacteria can be reused and embedded in advance. In the third edition of the design, we expanded the volume of the chamber to facilitate the subsequent development of the fluorescence test module. Taking into account the factors of liquid sealing, we abandoned the rubber ring structure and used hot pressing bonding to embed bacteria in advance. However, considering that the liquid may flow directly from the exhaust hole without passing through the culture chamber, we finally changed the position of the microchannel from the bottom of the first layer to the lower part of the second layer, so that the liquid can pass through the chamber before passing through the exhaust port, so as to obtain better liquid flow.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/engineering-success/engineering-2-chip-design-diagram-iteration-blanked-background.png" Label="Fig.7  Chip Design Diagram Iteration"/>

## **Build**

Before processing, the key functional parts had been simulated. The simulation results showed that the fluid distribution and the concentration gradient generator effect were consistent with the expectations. Then the chip was processed and the chip was constructed. The physical picture is shown on the right.

<div style="display: flex; flex-direction:row">

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/engineering-hardware-2.png" Label="Fig.8 Simulation Result Diagram" scale="1.1"/>

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/engineering-hardware-3.jpg" Label="Fig.9 Chip Physical Diagram" scale="0.5"/>

</div>


## **Tests**

We tested the function of the expected design. Firstly, the sampling process is tested, and the flow rate is controlled by peristaltic pump using visual edible pigment test. Subsequently, we made the engineered bacteria into a freeze-dried powder form, and tested the recovery of the engineered bacteria on the chip and the ability to respond to the damage reagent.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/engineering-hardware-4.png" Label="Fig.10 Concentration Gradient Diagram" scale="0.3"/>

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/t-hardware/pictures/hardware-14-3-bsbiaozheng.png" Label="Fig.11 NA Characterization of Freeze-Dried Bacteria"/>

## **Learn**

- After we tested each version, we recorded the corresponding error reasons, and communicated with the instructors in time to answer questions, gradually changing from PDMS material to PMMA, from the initial two-layer structure to the three-layer structure, and the functions of the chip were also improved.

- We also summarized the design scheme of thermal bonding for liquid seal and the scheme superior to adding rubber ring.

- The pressure balance and hydrophilic flow of the liquid flow should also be considered in order to obtain better fluidity and uniformity.

# Hardware Device

## **Aim**

- Create a dark box environment

- Adapt to the micro-fluidic chip to provide conditions for strain culture

- A POCT device, portable and miniaturized

## **Design** 

Our entire device is divided into three large parts, the first is the fuselage. The second is the drawer used to place the chip. The third part is the cover which can leave the mobile phone slot for shooting. In the second edition, we added the optical axis structure, the fuselage increased the side window operation storage bag, and the groove also increased the position of the countersunk screw hole. After the installation had been completed, the lid could be fixed and operated from the side window if necessary, and the lid could still be opened if necessary. It is worth mentioning that we have also added a handle design to the drawer, optimized the position of the pipe hole, and increased the fillet design. We made two holes on the edge of the chamber to facilitate injection.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/engineering-success/engineering-3-equipment-design-diagram-iteration-blanked-background.png" Label="Fig.12 Equipment Design Diagram Iteration"/>

## **Build**

We produced a pullable platform and fuselage covered with a chip through three-dimensional printing technology. We made two holes on the edge of the dark box to facilitate the injection. In the heating module, we choose PT1000 thermal sensor, silica gel heating film and TCM1040 small version thermostat to meet the demand. In the injection module part, we use the micro peristaltic pump to control and provide power, and the 304 stainless steel needle is used as the syringe.

## **Tests**

After assembling the initial version of the model, it was found that there were many problems after testing, such as unstable drawer sliding, inconvenient replacement of storage bags, etc. After summarizing, we furtherly improved the final version of the model, and the performance of all aspects of the final version of the model was excellent. After experiments, the fluorescence recognition was smooth, and the overall aesthetic appearance was greatly improved as well.

## **Learn**

In the process of equipment improvement, we have encountered various problems, such as insufficient stability, less aesthetically pleasing shape, low portability, etc. In each discussion and improvement process, we are recording in real time, summarizing the problems to find the help of instructors and advisors, constantly learning and improving and asking questions, and our relevant professional skills have also been greatly improved.