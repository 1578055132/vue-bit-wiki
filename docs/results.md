<Cover imgSrc="https://static.igem.wiki/teams/4691/wiki/head-9-result.png"/>


There are various environments with DNA damage risk and potential risk, and the factors causing DNA damage are also complicated. Additionally, even minimal damaging factors can pose a threat to the health of organisms over time due to their accumulation, requiring timely diagnose... These issues pose higher demands on the recognition range and sensitivity of sensors.

The HRP amplifier composed of hrpR/S and phrpL from Pseudomonas syringae amplifies the signal through the synthesis of a highly sensitive complex, activating downstream promoters to achieve high-gain signal output, and it possesses excellent modular performance.

Therefore, we added the HRP amplifier to the original circuit of recA-eGFP in order to enhance the sensitivity of our sensor.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/partcircuit-1.png" Label="Fig.1 Gene Circuit" scale="0.3"/>

# Strain construction

We obtained the recA-HRP-eGFP (hereafter referred to as HRP) circuit and the recA-eGFP (hereafter referred to as wild) circuit through PCR.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-1.png" Label="Fig.2 Electropherogram"/>

The gene circuits were ligated to the pUC19 vector using enzyme ligation and transformed into BL21 competent cells and MG1655 competent cells. The transformed cells were spread onto LB solid medium supplemented with ampicillin. Single colonies that grew were selected for amplification and sent to Jinweizhi (Suzhou) Biochemistry Co. for sequencing, and the remaining bacteria were preserved in glycerol.

# pre-experiment

Firstly, we performed screening of the host cells. The transformed wild-type bacteria were cultured on a shaker until the OD value reached 0.2, followed by the addition of NA for induction for 2 h.



<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-3.png" Label="Fig.3 Chassis Cell Selection"/>

As the concentration of NA increased, the fluorescence intensity of both engineering strains increased accordingly. The change was more pronounced in the BL21 strain, and the expression level of the fluorescence protein was much higher in the BL21 strain compared to the MG1655 strain.

The experiment indicates that the BL21 strain is more suitable for the expression of the recA circuit.

The overnight cultured HRP strain and wild-type strain were transferred into 30 mL fresh culture medium. Then, 30 μL of ampicillin was added, and the cultures were grown to the logarithmic growth phase. They were induced together under 1 μM Nalidixic Acid (NA), which inhibits bacterial DNA replication, and placed in a 37℃ shaker at 180 r/min. The OD value and fluorescence intensity were measured every 20 minutes during the induction process.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-4.png" Label="Fig.4 The OD Value of the Two Strains"/>

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-5.png" Label="Fig.5 The Fluorescence Intensity of the Two Strains"/>

The curves of RFU as a function of time can be calculated for both strains of bacteria.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-6.png" Label="Fig.6 The RFU of the Two Strains"/>

The RFU of both strains increases over time. The HRP strain has a higher baseline RFU than the wild-type strain and exhibits a faster growth rate. After 600 min, the RFU of the HRP strain reaches four times that of the wild-type strain.

The experimental results indicate the successful construction of the HRP amplifier, and the addition of the HRP amplifier significantly enhances the fluorescence expression levels in Escherichia coli.

# Sensitivity Testing of the Sensor

The overnight cultured HRP strain was transferred to 30 mL fresh culture medium. Then, 30 μL of ampicillin was added, and the cultures were grown to the logarithmic growth phase. The HRP strain was induced with different concentrations of NA for 2 hours to obtain the NA induction curve for the HRP strain.



<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-7.png" Label="Fig.7 The SFU of HRP strain"/>

From the curve, it can be inferred that the linear range of expression for the HRP strain under NA induction is approximately 1 to 5 μM.

The HRP strain and wild-type strain were separately induced with NA concentrations of 1 μM, 2 μM, 3 μM, 4 μM, and 5 μM for 2.5 hours. Linear equations were fitted for each induction condition.



<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-8.png" Label="Fig.8 The SFU of the Two Strains"/>

Under NA induction, the linear equation for fluorescence output of the HRP strain is y = 14234*x - 17794, and the linear equation for fluorescence output of the wild-type strain is 7510*x - 4368.

Using the formula LOD = 3σ/S, we calculated that the detection limit of the HRP sensor for NA is 0.01989 μM, which is 1.8 times higher than the detection limit of 0.03580 μM for the wild-type sensor.

# Characterization Experiment with Different Damaging Agents

We validated the recognition range and discrimination ability of the sensor using different reagents.

Hydrogen peroxide, commonly used for sterilization, can cause DNA damage in bacteria. Acetone, a low-toxicity chemical, can precipitate proteins. KANA, a commonly used antibiotic in laboratories, can inhibit bacterial protein synthesis.

The HRP strain was induced for 2 h under different concentrations of NA, H₂O₂, acetone, and KANA.



<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-9.png" Label="Fig.9 The SFU Values of the Four Reagents "/>

The experimental results show that the strain exhibits a good response to DNA-damaging agents such as NA and H₂O₂. DNA damage caused by H₂O₂ concentrations of 100 μM, 500 μM, and 1000 μM corresponds to DNA damage caused by NA concentrations of 2 μM, 3 μM, and 4 μM, respectively. On the other hand, non-damaging agents such as acetone and KANA do not elicit a response from the strain. These results demonstrate that the constructed sensor can respond to different DNA-damaging reagents and successfully differentiate between damaging and non-damaging agents.

# Testing with Different Types of Damage

In addition to testing chemical agents, we were curious to see if the sensor's detection capabilities could extend to include electromagnetic radiation with thermal effects, as well as the field of ionizing radiation with potential for development. Ultraviolet (UV) lamps are commonly used for sterilization in biological laboratories. UV radiation can directly affect the DNA of biological cells, causing damage and leading to bacterial death. We first selected UV lamps to induce bacterial DNA damage and tested the sensor's response to radiation.

The overnight cultured HRP strain and wild-type strain were transferred to 30 mL fresh culture medium. Then, 30 μL of ampicillin was added, and the cultures were grown to the logarithmic growth phase. The strains were exposed to UV light for 0 minutes, 1 minute, 2 minutes, 3 minutes, 4 minutes, and 5 minutes. Subsequently, they were placed in a 37℃ shaker at 180 rpm, and the OD value and fluorescence intensity were measured every 20 minutes.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-10.png" Label="Fig.10 The OD Value and Fluorescence Intensity of the Two Strains Exposed to UV Light for Different Time"/>

The OD values and fluorescence intensities of the two strains were taken after 1 min and 5 min of exposure to UV light. The RFU curves were calculated based on these measurements.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/result-11.png" Label="The RFU Value of the Two Strains Exposed for 1 Min and 5 Min"/>



The experimental results indicate that the sensor can detect DNA damage caused by UV radiation. Furthermore, the HRP amplifier can enhance gene expression in this scenario as well, with amplification factors of up to 3-fold and 1.7-fold observed under 1 minute and 5 minutes of exposure, respectively.

# Conclusion



- We successfully incorporated the HRP amplifier into the recA-eGFP circuit, leading to a maximum four-fold increase in bacterial fluorescence expression.
- Our sensor demonstrates improved sensitivity compared to the original circuit, with a detection limit of 0.01989 μM for NA.
- The sensor effectively differentiates between damaging and non-damaging agents.
- The constructed recA-HRP-eGFP sensor can respond to different types of damage, including radiation and chemical agents, indicating its potential for excellent performance in various practical scenarios.