
<Cover imgSrc="https://static.igem.wiki/teams/4691/wiki/head-1-human-practice.png" />

<style>

h2{
    font-weight: normal;
}
h3{
    font-weight: normal;
}
</style>

# Overview

​	The project of BIT this year is to develop an initiative DNA damage effect evaluation system, with the goal of providing a miniaturized, portable, integrated detection device that can complete DNA damage evaluation based on highly sensitive microbial sensors, and apply it to places with DNA damage risks, including ionizing radiation exposure areas, biochemical reagent exposure areas, etc. At the beginning of the project implementation, we consulted stakeholders and the public to develop products that are technically feasible and meet application requirements. We connected with industry experts, relevant practitioners, and the general public through online questionnaires, online meetings, and face-to-face meetings. We have organized a series of public speaking events for different groups, with the hope of enabling more people to understand the seriousness of DNA damage and the concept of solving problems based on synthetic biology, conveying the value of the project and considering its impact, seeking feedback and continuously improving our project.

# Work

## 1.Public Communication-questionnaire

​	After identifying the project goal as DNA damage effect detection this year, we designed a questionnaire to collect public opinions and suggestions to promote the project, with the aim of demonstrating the level of understanding, acceptance, and potential suggestions of ordinary people for DNA damage. The questionnaire complied with all legal and ethical requirements as well as the recommendations of iGEM, and a total of 242 valid questionnaires were collected.

​	The respondents of the questionnaire ranged from middle school students to people with over ten years of work experience. The first question was to understand everyone's familiarity with DNA damage. The results showed that about three-quarters of the respondents had heard of the concept of DNA damage, but only half of them knew that DNA damage is harmful to health (Figure1.left). After a simple science popularization, more than 77% of the respondents believed that it is necessary to prevent DNA damage (Figure1. right), which proves the significance of the project implementation.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/wenjuan1.jpg" Label="Fig.1 Results of question 1"/>

​	In the further investigation, it was found that 96.3% of the respondents wanted to know whether common products can cause DNA damage, and 57.9% of them wanted to know whether all chemical products they can be exposed to can cause DNA damage (Figure2. left). About using microorganisms as sensors to detect DNA damage, 81% of the respondents could accept the concept of microbial sensors, while 13.2% of them thought it was okay. Only 5.8% of the respondents could not accept the concept of microbial sensors (Figure2. right), mainly due to concerns about biological contamination issues. Therefore, the project introduced microfluidic chips in the design to provide a closed and safe reaction environment.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/wenjuan2.jpg" Label="Fig.2 Results of question 2"/>

​	During the questionnaire survey, we found that most people have a certain understanding of DNA damage, but are not very clear about its specific hazards and potential places. After receiving simple science popularization during the questionnaire filling process, almost everyone understood and accepted the importance of DNA damage effect evaluation, and provided several behaviors in daily life that may involve DNA damage risk (such as using inferior cosmetics, smoking, etc.), as well as suggestions for using the project. These helped us to pay extra attention to the prevention of biological safety when designing practical application scenarios for the project.

## 2.Stakeholder consultation-online/face-to-face meetings

### Experts interviews

-Dr. Jinfeng Zhang:a professor at Beijing Institute of Technology

​	To understand the engineering effectiveness of developing DNA damage detection based on microbial sensors, we conducted a face-to-face interview with Professor Zhang Jinfeng, who specializes in organic chemistry and chemical engineering technology. Professor Zhang believed that the project is very meaningful in detecting targets, as most substances can cause DNA damage to living organisms, especially some chemical products. She advised us to choose the next research scenario and consulting objects. In addition, Professor Zhang pointed out that we should not only focus on the sensitivity and accuracy of the detection, but also pay attention to the detection time to improve the application value. Her viewpoint guided our biological circuit design.

-Dr. Xiaoming Wu:10 years of experience in chemical research and development

​	To determine the application value of the project and whether it is beneficial to the world, we consulted Dr. Wu Xiaoming, who has over 10 years of experience in chemical product research and development, in a face-to-face meeting. We hoped that he could evaluate the significance of our project from a professional perspective. Dr. Wu stated that some organic compounds found in chemical products can easily cause DNA damage, which is a phenomenon that the industry has been paying close attention to. This phenomenon has also become an assessment criterion for the launch of new drugs in the past five years. For example, the withdrawn drug sartan contains a DNA damage-inducing impurity NDMA. Therefore, he firmly believes that our project is beneficial to society and has great application value in the pharmaceutical market. In addition, Dr. Wu pointed out that the industry believes that there are five grades of toxic compounds that can cause DNA damage. If the detection throughput can be improved while assessing the risk of damage, it will save manpower and resources consumed during the detection process. 	This viewpoint inspired our subsequent microfluidic chip structural design.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/wxm.jpg" Label="Fig.3 Dr. Xiaoming Wu"/>

-Dr.Zhonglian Zhang:a professor at Beijing Institute of Technology

​	After understanding the application value and scenarios of the project, we decided to combine microfluidic chips with miniaturized devices to achieve multi-scenario DNA damage detection. With the aforementioned views, we interviewed Professor Zhang Zhonglian, who has years of experience in instrument and sensor preparation. Professor Zhang praised the concept of our project, especially the integration of microfluidic chips and small-scale equipment, and the use of smartphones to analyze results, which possess advantages of portability and flexibility. Professor Zhang believed that potential users of the product include personnel who are exposed to harmful substances such as chemical products, and they need intuitive, portable, and easy-to-use testing equipment to provide them with a preliminary testing method. Based on the test results, they can take preventive measures. Additionally, Professor Zhang provided us with some suggestions for modifying the instrument (detailed in integrated HP) to help us determine the final structure of the instrument. The potential users proposed by Professor Zhang were consistent with our expectations. Based on the above views, we interviewed two former team members who are currently engaged in the development of biochemical reagents.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/zhangjiaoshou.png" Label="Fig.4 Dr.Zhonglian Zhang"/>

### Relevant practitioners interviews

-Wu Minjie:Biochemical Product R&D Engineer

​	Wu Minjie, coach of the 2019 BIT team, currently working at Guangzhou Wondfo Biotech, the work content is the production of biochemical products, there is a risk of exposure to organic chemical reagents, we conducted an interview in the form of an online meeting. Wu Minjie believes that there are many methods for detecting DNA damage at present, such as many companies studying DNA damage kits, as well as ELISA, electrophoresis, simple chromatography, etc., but these methods are relatively expensive to detect, and the operation requirements are high, requiring professional people to carry out in a laboratory environment, the project has obvious advantages, she will tend to use this project product, and at the same time think that the product can be tested for users who use biochemical reagent products.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/xuejie.jpg" Label="Fig.5 Online interviews 1"/>

-Pengzhao:Biochemical Product R&D Engineer

​	Peng Zhao, the captain of the 2018 BIT team, currently works at Shenzhen Snibe Diagnostic, designing chemical synthesis, and also interviewed Peng Zhao in the form of an online conference. Peng Zhao believes that workers like them who have frequent contact with biological and chemical reagents will need to use such equipment to ensure safety and health, and this equipment can be applied to the risk assessment of daily radiation environment, based on microbial sensors for detection, which is a supplement to physical and chemical methods, which is a great innovation, and he is very willing to promote our products.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/pengzhao.jpg" Label="Fig.6 Online interviews 2"/>

​	By consulting stakeholders,industry experts, university professors and related industry workers to effectively teach, we firmly believe that this year's project has certain value, through the establishment of active DNA damage detection sensors, with portable, automated equipment, can be adapted to different scenarios of different personnel, such as providing staff engaged in biochemical reagent product research and development, ionizing radiation exposure areas and other use, has important social significance. At the same time, the relevant test results can be disclosed to scientists in related fields for research, promoting the development of DNA damage effect research. Through consulting professionals, the four links of design, technology, implementation and application of the project were revised, and this year's project was finally determined.
 	For more details of interviews,please visit Integrated Human Practices

## 3.Value spreading-organize presentations

​	After determining the application value of the project, we hope to spread the understanding and implementation effect of the project to more people, expect to get feedback from it, and pass on the concept of iGEM and synthetic biology to everyone, so that more people can feel the charm of synthetic biology and solve real-world problems. We have carried out a series of publicity activities in a face-to-face format, and adjusted the content of the propaganda to achieve the best publicity effect when facing four different groups: elementary school students, junior high school students, university students and social groups.

### Elementary school students - story guided, edutainment

​	For elementary school students, it is important to understand synthetic biology, roughly understand what DNA is, and the risk of DNA damage that may be encountered in life, and finally let elementary school students make cells out of clay in groups and experience the beauty of biology firsthand.

<!-- 轮播图2 -->
<CarouselFigure/>

### Middle school students - from shallow to deep, brainstorming

​	The middle school student group has a certain biological knowledge foundation. The presentation is divided into synthetic biology introduction and team project introduction, synthetic biology and their current knowledge to learn, and use the inspiration, design and implementation process of this year's BIT team project, to give them a more vivid display of how to use synthetic biology to solve real problems, stimulate everyone's interest in synthetic biology. After the conference we created an iGEM exchange group. I believe they will become a new star in the field of synthetic biology in the future.


<!-- 轮播图3 -->
<cfigure2/>

### **College students - introduce more, get feedback**

​	For college students, we held a series of lectures and jointly launched the "iG20" activity with more than 20 universities, including Lanzhou University. We introduced the content of this year's project and the knowledge of synthetic biology, discussed the application cases of synthetic biology related to human health, environmental protection and sustainable development, successfully promoted the project value, synthetic biology concept and iGEM value, and obtained feedback from college students and iGEM teams from various universities, which triggered our team's thinking on future applications.


<!-- 轮播图4 -->
<cfigure3/>

### Social groups - popular science lectures, propaganda value

​	Leaving campus, we expanded our outreach to social groups, including supermarket shopping guides, dormitory aunts and random passers-by. In short, we are constantly working to expand the influence and awareness of synthetic biology.


<cfigure4/>

## 4.Safety promotion-Game design

​	Safety is the most important part of the production process. The officials of iGEM also pay lots of attention to it and have taken many measures to ensure it. While filling out the safety form and taking laboratory safety training, we recognize that an efficient and fast safety training method, can enable more people to recognize the importance of safety, and at the same time receive some fundamental training, reducing the occurrence of safety accidents. In order to meet that vision, we designed an interactive dialogue game.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/game-version1.jpg" Label="Fig.7 Game-Version1"/>

​	Participants can easily complete the learning of safety by advancing the plot of the game. In order to ensure effectiveness, the game content consulted the laboratory teacher and a literary writer, Ms. Wang Yan.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/game-interview.jpg" Label="Fig.8 The interview for game design"/>

​	The game has been released on the iGEM community and other online platforms, continuing collecting feedbacks, has been developed version 2.0, hoping to make many people pay attention to laboratory safety and avoid safety accidents through the promotion of the game.

<ImageAndLabel3 ImageSrc="https://static.igem.wiki/teams/4691/wiki/humanpractice/game-version2.jpg" Label="Fig.9 Game-Version2&acclaim"/>

# Summary

​	On the Human Practices page, we fully demonstrate the value of the project to society and the world. We learn more about the ideas of the public and stakeholders, modify the project after obtaining feedback, and identify the use cases and audiences. Based on the concept of the project, we carried out a series of publicity activities to promote the value of the project and synthetic biology, so that more people can understand DNA damage detection and synthetic biology, and finally we launched a safety mini-game to let more people systematically understand the importance of safety, get systematic safety training, and make our contribution to society.