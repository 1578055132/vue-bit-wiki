---
sidebar: false
---

<Cover imgSrc="https://static.igem.wiki/teams/4691/wiki/head-6-attributions.png" alt/>

<iframe
style='height:2300px; width: 100%'
id="igem-attribution-form"
src="https://attributions.igem.org/?year=2023&team=BIT">
</iframe>
